/*
 * PDF-Stamp
 * Copyright (C) 2017-2024 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.stamp.service;

import com.itextpdf.kernel.exceptions.BadPasswordException;
import com.itextpdf.kernel.exceptions.PdfException;
import com.itextpdf.kernel.pdf.*;
import coop.libriciel.stamp.error.RestInvalidPdfException;
import coop.libriciel.stamp.util.DocumentOpenedListener;
import lombok.NonNull;
import lombok.extern.log4j.Log4j2;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.util.UUID;
import java.util.stream.IntStream;

import static org.springframework.http.HttpStatus.OK;
import static org.springframework.http.MediaType.APPLICATION_PDF;


@Log4j2
public class HttpResponseUtils {


    /**
     * This method uses a temp file, that can be avoided.
     * The PDF document can write directly on the httpServlet Output.
     *
     * @param file                 the original file
     * @param withUnethicalReading recursive parameter, used on locked PDF
     * @param appendMode           recursive parameter, used on poorly-formatted PDF
     * @param callback             the document used to stamp/comment
     * @return a ResponseEntity
     * @throws IOException             sent if something went wrong reading/writing http request
     * @throws RestInvalidPdfException sent when even the recursive parameters has fail
     */
    @Deprecated
    public static ResponseEntity<InputStreamResource> handleFile(@NonNull MultipartFile file,
                                                                 boolean withUnethicalReading,
                                                                 boolean appendMode,
                                                                 @NonNull DocumentOpenedListener callback) throws IOException, RestInvalidPdfException {

        log.debug("handleFile name:{} unethicalReading:{} appendMode:{}", file, withUnethicalReading, appendMode);

        // Create temp file to reduce memory usage
        File tmpFile = File.createTempFile(UUID.randomUUID().toString(), ".tmp");
        log.info("Stamping document %s as tmp file %s".formatted(file.getOriginalFilename(), tmpFile.getName()));

        try (FileOutputStream out = new FileOutputStream(tmpFile);
             PdfReader reader = new PdfReader(file.getInputStream())) {

            StampingProperties properties = new StampingProperties().preserveEncryption();
            if (appendMode) {
                properties.useAppendMode();
            }

            // If a password was needed to edit this PDF...
            // We have to make "bad" things :(
            reader.setUnethicalReading(withUnethicalReading);

            try (PdfWriter writer = new PdfWriter(out);
                 PdfDocument document = new PdfDocument(reader, writer, properties)) {

                // Actual operation
                callback.onDocumentLoaded(document);

            } catch (BadPasswordException e) {
                if (!withUnethicalReading) {
                    log.warn("%s is protected by a password. Begin unethical reading...".formatted(file.getOriginalFilename()));
                    return handleFile(file, true, appendMode, callback);
                } else {
                    log.error("%s is protected by a password".formatted(file.getOriginalFilename()));
                    throw new RestInvalidPdfException(e.getLocalizedMessage());
                }
            }

        } catch (PdfException e) {
            if (appendMode) {
                log.warn("%s is not a valid PDF file, and will be stamped in non append mode - signature(s) will be invalidated"
                        .formatted(file.getOriginalFilename()));

                return handleFile(file, withUnethicalReading, false, callback);
            } else {
                log.error("%s is not a valid PDF file".formatted(file.getOriginalFilename()));
                throw new RestInvalidPdfException(e.getLocalizedMessage());
            }
        } catch (com.itextpdf.io.exceptions.IOException e) {
            log.error("File %s is not a PDF file".formatted(file.getOriginalFilename()), e);
            throw new RestInvalidPdfException(e.getLocalizedMessage());
        }

        log.info("Stamping done for %s".formatted(tmpFile.getName()));

        // Set response headers
        HttpHeaders respHeaders = new HttpHeaders();
        respHeaders.setContentType(APPLICATION_PDF);
        respHeaders.setContentLength(tmpFile.length());
        respHeaders.setContentDispositionFormData("attachment", "StampedPDF.pdf");

        // Define InputStream to return request
        InputStreamResource isr = new InputStreamResource(new FileInputStream(tmpFile));

        try {
            Files.delete(tmpFile.toPath());
        } catch (IOException e) {
            log.error("Cannot delete tmp file", e);
        }

        return new ResponseEntity<>(isr, respHeaders, OK);
    }


    /**
     * Cleaner method, that outputs directly the request.
     *
     * @param file                 the original file
     * @param withUnethicalReading recursive parameter, used on locked PDF
     * @param appendMode           recursive parameter, used on poorly-formatted PDF
     * @param callback             the document used to stamp/comment
     * @throws IOException             sent if something went wrong reading/writing http request
     * @throws RestInvalidPdfException sent when even the recursive parameters has fail
     */
    public static void handleFile(@NonNull MultipartFile file,
                                  boolean withUnethicalReading,
                                  boolean appendMode,
                                  @NonNull DocumentOpenedListener callback,
                                  PdfWriter writer) throws IOException, RestInvalidPdfException {

        log.debug("handleFile name:{} unethicalReading:{} appendMode:{}", file, withUnethicalReading, appendMode);

        try (PdfReader reader = new PdfReader(file.getInputStream())) {

            StampingProperties properties = new StampingProperties().preserveEncryption();
            if (appendMode) {
                properties.useAppendMode();
            }

            // If a password was needed to edit this PDF...
            // We have to make "bad" things :(
            reader.setUnethicalReading(withUnethicalReading);

            try (PdfDocument document = new PdfDocument(reader, writer, properties)) {

                // Actual operation
                callback.onDocumentLoaded(document);

                // If a page is modified, we have to 'mark' all other pages as modified too,
                // or some pages will return as invalid for Adobe Reader.
                // So... Long story short : we mark every page as modified.
                IntStream.rangeClosed(1, document.getNumberOfPages())
                        .mapToObj(document::getPage)
                        .forEach(PdfObjectWrapper::setModified);

            } catch (BadPasswordException e) {
                if (!withUnethicalReading) {
                    log.warn("%s is protected by a password. Begin unethical reading...".formatted(file.getOriginalFilename()));
                    handleFile(file, true, appendMode, callback, writer);
                    return;
                } else {
                    log.error("%s is protected by a password".formatted(file.getOriginalFilename()));
                    throw new RestInvalidPdfException(e.getLocalizedMessage());
                }
            }

        } catch (PdfException | com.itextpdf.io.exceptions.IOException e) {
            if (appendMode) {
                log.warn("%s is not a valid PDF file, and will be stamped in non append mode - signature(s) will be invalidated"
                        .formatted(file.getOriginalFilename()));
                handleFile(file, withUnethicalReading, false, callback, writer);
                return;
            } else {
                log.error("%s is not a valid PDF file".formatted(file.getOriginalFilename()));
                throw new RestInvalidPdfException(e.getLocalizedMessage());
            }
        }

        log.debug("Stamping done for %s".formatted(file.getName()));
    }


}
