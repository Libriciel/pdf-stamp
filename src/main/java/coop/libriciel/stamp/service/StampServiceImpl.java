/*
 * PDF-Stamp
 * Copyright (C) 2017-2024 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.stamp.service;

import com.itextpdf.io.image.ImageData;
import com.itextpdf.io.image.ImageDataFactory;
import com.itextpdf.kernel.colors.WebColors;
import com.itextpdf.kernel.font.PdfFont;
import com.itextpdf.kernel.font.PdfFontFactory;
import com.itextpdf.kernel.geom.Rectangle;
import com.itextpdf.kernel.geom.Vector;
import com.itextpdf.kernel.pdf.*;
import com.itextpdf.kernel.pdf.annot.*;
import com.itextpdf.kernel.pdf.canvas.PdfCanvas;
import com.itextpdf.kernel.pdf.extgstate.PdfExtGState;
import com.itextpdf.kernel.pdf.xobject.PdfFormXObject;
import com.itextpdf.layout.Canvas;
import com.itextpdf.layout.element.Image;
import coop.libriciel.stamp.model.*;
import coop.libriciel.stamp.util.DocumentUtils;
import coop.libriciel.stamp.util.FontUtils;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.IntStream;

import static com.itextpdf.io.font.PdfEncodings.CP1252;
import static com.itextpdf.io.font.constants.StandardFonts.HELVETICA;
import static com.itextpdf.kernel.colors.ColorConstants.BLACK;
import static com.itextpdf.kernel.colors.ColorConstants.WHITE;
import static com.itextpdf.kernel.pdf.PdfName.Text;
import static coop.libriciel.stamp.util.RectangleRotationUtil.rotateRectangle;
import static java.util.Comparator.naturalOrder;
import static org.apache.commons.lang3.ObjectUtils.allNotNull;
import static org.springframework.http.HttpStatus.NOT_FOUND;


@Service
@Log4j2
public class StampServiceImpl implements StampService, AnnotationVisitor {


    // <editor-fold desc="Beans">


    private final TextFinderService textFinderService;


    @Autowired
    public StampServiceImpl(TextFinderService textFinderService) {
        this.textFinderService = textFinderService;
    }


    // </editor-fold desc="Beans">


    // <editor-fold desc="AnnotationVisitor">


    @Override
    public float getWidth(@NotNull Stamp stamp, @Nullable ImageData imageData) {
        if (stamp.getPosition().getWidth() != 0) {
            // Forced width, defined by the user, we respect it
            return stamp.getPosition().getWidth();
        } else if (imageData != null) {
            // Automatic size on image
            float imageWidth = imageData.getWidth(); // TODO: * (72F / imageData.getDpiX());
            stamp.getPosition().setWidth((int) imageWidth);
            return imageWidth;
        } else {
            // Automatic size on text :
            // the longer text line will do
            float textWith = stamp.getRows().stream()
                    .map(r -> FontUtils.getFontWidth(r.getTitle() + " " + r.getValue(), stamp.getFontSize()) + stamp.getPosition().getMarginLeft() * 2)
                    .max(naturalOrder())
                    .orElse(0f);
            stamp.getPosition().setWidth((int) textWith);
            return textWith;
        }
    }


    @Override
    public float getWidth(@NotNull Comment comment, @Nullable ImageData imageData) {
        return comment.getPosition().getWidth();
    }


    @Override
    public float getHeight(@NotNull Stamp stamp, @Nullable ImageData imageData) {
        if (stamp.getPosition().getHeight() != 0) {
            // Forced width, defined by the user, we respect it
            return stamp.getPosition().getHeight();
        } else if (imageData != null) {
            // Automatic size on image
            float imageHeight = imageData.getHeight(); // TODO: * (72F / imageData.getDpiY());
            stamp.getPosition().setHeight((int) imageHeight);
            return imageHeight;
        } else {
            // Automatic size on text :
            // the line-height times the number of lines will do
            float textHeight = FontUtils.getMaxHeight(stamp.getFontSize(), stamp.getRows().size(), 0, 0, stamp.getSpacesBetweenLines());
            stamp.getPosition().setHeight((int) textHeight);
            return textHeight;
        }
    }


    @Override
    public float getHeight(@NotNull Comment comment, @Nullable ImageData imageData) {
        return comment.getPosition().getHeight();
    }


    @Override
    public @NotNull PdfMarkupAnnotation generatePdfMarkupAnnotation(@NotNull Stamp stamp, Rectangle position) {
        return new PdfStampAnnotation(position);
    }


    @Override
    public @NotNull PdfMarkupAnnotation generatePdfMarkupAnnotation(@NotNull Comment comment, Rectangle position) {
        PdfTextAnnotation result = new PdfTextAnnotation(position);

        Optional.ofNullable(comment.getContents()).ifPresent(result::setContents);
        Optional.ofNullable(comment.getName()).ifPresent(s -> result.setName(new PdfString(s)));
        Optional.ofNullable(comment.getTitle()).ifPresent(s -> result.setTitle(new PdfString(s)));

        return result;
    }


    @Override
    public @NotNull PdfFormXObject createPdfFormXObject(@NotNull PdfDocument document, @NotNull Stamp stamp) {

        boolean singleRow = stamp.getRows().size() == 1;
        boolean logoOnly = stamp.getRows().stream().anyMatch(
                row -> row.getLogo() != null && row.getTitle() == null && row.getValue() == null
        );
        boolean standaloneLogo = singleRow && logoOnly;

        ImageData singleImageData = null;
        Logo singleLogo = null;
        if (standaloneLogo) {
            singleLogo = stamp.getRows().stream()
                    .map(Row::getLogo)
                    .filter(Objects::nonNull)
                    .filter(l -> l.getImageData() != null)
                    .findFirst()
                    .orElse(null);

            if (singleLogo != null) {
                try (InputStream imageInputStream = singleLogo.getImageData()) {
                    singleImageData = createImage(imageInputStream.readAllBytes());
                } catch (com.itextpdf.io.exceptions.IOException | IOException e) {
                    log.error("Bad image format", e);
                }
            }
        }

        // V5 case

        Rectangle rect;
        if (standaloneLogo && singleImageData != null) {
            rect = getDimensionsForImage(singleImageData, stamp.getPosition());
        } else {
            float textWidth = stamp.getPosition().getWidth() != 0
                    ? stamp.getPosition().getWidth()
                    : stamp.getRows().stream()
                        .map(r -> FontUtils.getFontWidth(r.getTitle() + " " + r.getValue(), stamp.getFontSize())
                                    + stamp.getPosition().getMarginLeft() * 2)
                        .max(naturalOrder())
                        .orElse(0f);
            stamp.getPosition().setWidth((int) textWidth);

            float textHeight = stamp.getPosition().getHeight() != 0
                    ? stamp.getPosition().getHeight()
                    : FontUtils.getMaxHeight(stamp.getFontSize(), stamp.getRows().size(), 0, 0, stamp.getSpacesBetweenLines());
            stamp.getPosition().setHeight((int) textHeight);

            rect = new Rectangle(textWidth, textHeight);
        }

        log.debug("createPdfFormXObject - width : {}, height : {}", rect.getWidth(), rect.getHeight());

        PdfFormXObject template = new PdfFormXObject(rect);
        PdfCanvas canvas = new PdfCanvas(template, document);
        PdfExtGState gs1 = new PdfExtGState()
                .setFillOpacity(stamp.getOpacity())
                .setStrokeOpacity(stamp.getOpacity());

        canvas.saveState().setExtGState(gs1);

        if (standaloneLogo && singleImageData != null) {

            Rectangle imgRect = getDimensionsForImage(singleImageData, singleLogo);
            canvas.addImageFittedIntoRectangle(singleImageData, imgRect, false);

        } else {

            if (stamp.isDrawRectangle()) {
                canvas.rectangle(rect)
                        .setStrokeColor(BLACK)
                        .setFillColor(WHITE)
                        .setLineWidth(2)
                        .fillStroke();
            }

            for (int i = 0; i < stamp.getRows().size(); i++) {
                Row r = stamp.getRows().get(i);

                float yPos = FontUtils.getMaxHeight(
                        stamp.getFontSize(),
                        stamp.getRows().size(),
                        i + 1,
                        stamp.getPosition().getHeight(),
                        stamp.getSpacesBetweenLines()
                );
                addText(
                        canvas,
                        stamp.getFontSize(),
                        stamp.getPosition().getMarginLeft(),
                        yPos,
                        r
                );

                if (r.getLogo() != null && r.getLogo().getImageData() != null) {
                    try (InputStream imageInputStream = r.getLogo().getImageData()) {
                        ImageData imageData = createImage(imageInputStream.readAllBytes());

                        Rectangle imgRect = getDimensionsForImage(imageData, r.getLogo());

                        int xPos = stamp.getPosition().getWidth() - r.getLogo().getMarginRight() - (int) imgRect.getWidth();
                        imgRect.setX(xPos);
                        imgRect.setY(yPos);
                        canvas.addImageFittedIntoRectangle(imageData, imgRect, false);
                    } catch (com.itextpdf.io.exceptions.IOException | IOException e) {
                        log.error("Bad image format, stamp row won't be applied : {}", e.getMessage());
                        log.debug("Error details : ", e);
                    }
                }
            }
        }

        canvas.restoreState().release();
        return template;
    }

    @NotNull
    private static Rectangle getDimensionsForImage(@NotNull ImageData imageData, @NotNull Dimensions inputParamDimensions) {

        int targetImgWidth = (int) imageData.getWidth();
        int targetImgHeight = (int) imageData.getHeight();

        if (inputParamDimensions.getWidth() != 0) {
            targetImgWidth = inputParamDimensions.getWidth();
            if (inputParamDimensions.getHeight() != 0) {
                targetImgHeight = inputParamDimensions.getHeight();
            } else {
                double scale = targetImgWidth / imageData.getWidth();
                targetImgHeight = (int) (imageData.getHeight() * scale);
            }
        }

        return new Rectangle(targetImgWidth, targetImgHeight);
    }


    @Override
    public @NotNull PdfFormXObject createPdfFormXObject(@NotNull PdfDocument document, @NotNull Comment comment) {
        Rectangle rect = new Rectangle(0, 0, getWidth(comment, null), getHeight(comment, null));

        PdfFormXObject template = new PdfFormXObject(rect);
        PdfCanvas canvas = new PdfCanvas(template, document);
        PdfExtGState gs1 = new PdfExtGState()
                .setFillOpacity(comment.getOpacity())
                .setStrokeOpacity(comment.getOpacity());

        canvas.saveState().setExtGState(gs1);

        byte[] commentIconBytes;
        try (InputStream iconInputStream = comment.getImageData()) {
            commentIconBytes = iconInputStream.readAllBytes();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        ImageData img = ImageDataFactory.create(commentIconBytes);
        canvas.addImageAt(img, 0, 0, false);

        canvas.restoreState().release();

        return template;
    }


    // </editor-fold desc="AnnotationVisitor">


    // <editor-fold desc="StampService">


    @Override
    public void addAnnotation(PdfDocument document, List<? extends Annotation> stamps) {
        stamps.forEach(s -> addAnnotation(document, s));
    }


    @Override
    public void deleteAnnotation(PdfDocument document, String annotationName) {

        AtomicBoolean found = new AtomicBoolean(false);

        for (int pageIndex = 1; pageIndex <= document.getNumberOfPages(); pageIndex++) {
            int finalPageIndex = pageIndex;

            document.getPage(pageIndex).getAnnotations().stream()
                    .filter(a -> allNotNull(a, a.getSubtype(), a.getName()))
                    .filter(a -> a.getSubtype() == Text)
                    .filter(a -> StringUtils.equals(a.getName().toString(), annotationName))
                    .forEach(a -> {
                        log.info("Deleting annotationName:{}", a.getName());
                        found.set(true);
                        document.getPage(finalPageIndex).removeAnnotation(a);
                    });
        }

        // Apparently, we have to mark every page as modified, if something was done.

        if (found.get()) {
            IntStream.range(1, document.getNumberOfPages())
                    .forEach(pageIndex -> document.getPage(pageIndex).setModified());
        } else {
            throw new ResponseStatusException(NOT_FOUND, "AnnotationId not found in given PDF");
        }
    }


    // </editor-fold desc="StampService">


    /**
     * Add a text line to the template
     *
     * @param template The template where to add the text line
     * @param size     font's size
     * @param x        X position
     * @param y        Y position
     * @param row      row number (for better Y positioning)
     */
    private void addText(PdfCanvas template, int size, int x, float y, Row row) {

        String textToShow = (row.getTitle() != null ? row.getTitle() : "") + " " + (row.getValue() != null ? row.getValue() : "");
        template.beginText();

        PdfFont helveticaFont;
        try {
            helveticaFont = PdfFontFactory.createFont(HELVETICA, CP1252);
        } catch (IOException e) {
            log.error("Error instantiating font", e);
            throw new RuntimeException(e);
        }

        template.setFontAndSize(helveticaFont, size);
        template.setFillColor(WebColors.getRGBColor(row.getColor()));
        template.moveText(x, y);
        template.showText(textToShow);
        template.endText();
    }


    /**
     * @throws com.itextpdf.io.exceptions.IOException The ImageDataFactory can throw an IOException on bad data. Yet it does not declare any throws.
     *                                                We have to do add it manually, then.
     */
    @SuppressWarnings("RedundantThrows")
    private ImageData createImage(byte[] image) throws com.itextpdf.io.exceptions.IOException {
        return ImageDataFactory.create(image);
    }


    private void addStampWithTemplateOnPage(PdfDocument document, Annotation annotation, int pageNum, PdfFormXObject template) {

        PdfPage page = document.getPage(pageNum);
        int orientation = page.getRotation();

        Image image = new Image(template);
        image.setRotationAngle(orientation * Math.PI / 180);

        Rectangle position = new Rectangle(
                annotation.getPosition().getX(),
                annotation.getPosition().getY(),
                template.getWidth(),
                template.getHeight()
        );

        // Eventually center the image on "position" X and Y
        handleCenteredPositioning(position, image, annotation);

        Rectangle cropBox = page.getCropBox();
        Rectangle mediaBox = page.getMediaBox();

        if (!mediaBox.contains(cropBox) && !mediaBox.equalsWithEpsilon(cropBox, 0.5f)) {
            log.warn("Odly formed pdf : the cropbox seems to be bigger than the mediabox." +
                    " Cropbox will be ignored, and mediabox used instead.");
            cropBox = mediaBox;
        }

        // Rotate the rectangle with the page, not handled by default on itext, we have to do it manually
        position = rotateRectangle(position, cropBox, orientation, annotation.getPosition().getOrigin());

        // Find specific text when position is "onText"
        handleOnTextPositioning(position, image, document, annotation, pageNum, orientation);

        template = new PdfFormXObject(new Rectangle(position.getWidth(), position.getHeight()));

        PdfCanvas canvas = new PdfCanvas(template, document);
        canvas.saveState();

        try (Canvas imageCanvas = new Canvas(canvas, new Rectangle(position.getWidth(), position.getHeight()))) {
            imageCanvas.add(image);
        }

        canvas.restoreState();
        canvas.release();

        PdfMarkupAnnotation markupAnnotation = annotation.generatePdfMarkupAnnotation(this, position);
        markupAnnotation.setAppearance(PdfName.N, new PdfAnnotationAppearance(template.getPdfObject()));
        markupAnnotation.setFlags(PdfAnnotation.PRINT);

        page.addAnnotation(markupAnnotation);
        page.setModified();
    }


    private void handleOnTextPositioning(Rectangle position, Image image, PdfDocument document, Annotation annotation, int pageNum, int orientation) {

        if (annotation.getPosition().getOnText() != null) {
            List<Vector> vectors = textFinderService.locateText(document, pageNum, annotation.getPosition().getOnText());

            if (!vectors.isEmpty()) {

                float x = vectors.get(0).get(0);
                float y = vectors.get(0).get(1);

                if (orientation == 90 || orientation == 270) {
                    position.setX(x - image.getImageScaledHeight() / 2);
                    position.setY(y - image.getImageScaledWidth() / 2);
                } else {
                    position.setX(x - image.getImageScaledWidth() / 2);
                    position.setY(y - image.getImageScaledHeight() / 2);
                }
            }
        }
    }


    private void handleCenteredPositioning(Rectangle position, Image image, Annotation annotation) {
        if (annotation.getPosition().isCentered()) {
            position.setX(position.getX() - image.getImageScaledWidth() / 2);
            position.setY(position.getY() - image.getImageScaledHeight() / 2);
        }
    }


    private void applyTemplate(PdfDocument document, Annotation annotation, PdfFormXObject template) throws IOException {
        int numberOfPages = document.getNumberOfPages();

        Integer computedPage = DocumentUtils.computePdfPageNumber(numberOfPages, annotation.getPage());
        if ((computedPage == null) || (numberOfPages == 0)) {
            return;
        }

        // If a page is modified, we have to 'mark' all other pages as modified,
        // or some pages will return as invalid for Adobe Reader
        IntStream.rangeClosed(1, numberOfPages)
                .filter(pageNumber -> (computedPage == pageNumber) || (computedPage == 0))
                .forEach(pageNumber -> addStampWithTemplateOnPage(document, annotation, pageNumber, template));
    }


    private void addAnnotation(PdfDocument document, Annotation annotation) {

        // Create template for annotation
        try {
            PdfFormXObject template = annotation.createPdfFormXObject(this, document);
            applyTemplate(document, annotation, template);
        } catch (IOException e) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Something went wrong adding Stamp", e);
        }
    }


}
