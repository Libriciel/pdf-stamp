/*
 * PDF-Stamp
 * Copyright (C) 2017-2024 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.stamp.model;

import com.itextpdf.io.image.ImageData;
import com.itextpdf.kernel.geom.Rectangle;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.annot.PdfMarkupAnnotation;
import com.itextpdf.kernel.pdf.xobject.PdfFormXObject;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.List;


@Data
@EqualsAndHashCode(callSuper = true)
public class Stamp extends Annotation {


    private List<Row> rows = new ArrayList<>();
    private int fontSize;
    private int spacesBetweenLines;
    private boolean drawRectangle = true;


    // <editor-fold desc="Visitor pattern">


    @Override
    public float getWidth(@NotNull AnnotationVisitor v, @Nullable ImageData imageData) {
        return v.getWidth(this, imageData);
    }


    @Override
    public float getHeight(@NotNull AnnotationVisitor v, @Nullable ImageData imageData) {
        return v.getHeight(this, imageData);
    }


    @Override
    public @NotNull PdfMarkupAnnotation generatePdfMarkupAnnotation(@NotNull AnnotationVisitor v, @NotNull Rectangle rectangle) {
        return v.generatePdfMarkupAnnotation(this, rectangle);
    }


    @Override
    public @NotNull PdfFormXObject createPdfFormXObject(@NotNull AnnotationVisitor v, @NotNull PdfDocument document) {
        return v.createPdfFormXObject(document, this);
    }


    // </editor-fold desc="Visitor pattern">

}
