/*
 * PDF-Stamp
 * Copyright (C) 2017-2024 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.stamp.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.itextpdf.io.image.ImageData;
import com.itextpdf.kernel.geom.Rectangle;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.annot.PdfMarkupAnnotation;
import com.itextpdf.kernel.pdf.xobject.PdfFormXObject;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.InputStream;


@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@JsonIgnoreProperties(ignoreUnknown = true)
public class Comment extends Annotation {


    private String name;
    private String title;
    private String contents;
    private @Deprecated String iconBase64; // Api v1/v2
    private @JsonIgnore InputStream imageData;
    private CommentIcon icon;


    // <editor-fold desc="Visitor pattern">


    @Override
    public float getWidth(@NotNull AnnotationVisitor v, @Nullable ImageData imageData) {
        return v.getWidth(this, imageData);
    }


    @Override
    public float getHeight(@NotNull AnnotationVisitor v, @Nullable ImageData imageData) {
        return v.getHeight(this, imageData);
    }


    @Override
    public @NotNull PdfMarkupAnnotation generatePdfMarkupAnnotation(@NotNull AnnotationVisitor v, @NotNull Rectangle rectangle) {
        return v.generatePdfMarkupAnnotation(this, rectangle);
    }


    @Override
    public @NotNull PdfFormXObject createPdfFormXObject(@NotNull AnnotationVisitor v, @NotNull PdfDocument document) {
        return v.createPdfFormXObject(document, this);
    }


    // </editor-fold desc="Visitor pattern">

}
