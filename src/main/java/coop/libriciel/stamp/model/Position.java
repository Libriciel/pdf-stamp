/*
 * PDF-Stamp
 * Copyright (C) 2017-2024 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.stamp.model;

import com.fasterxml.jackson.annotation.JsonAlias;
import lombok.Data;

import static coop.libriciel.stamp.model.PositionOrigin.TOP_RIGHT;


@Data
public class Position implements Dimensions {

    private int x;
    private int y;
    private int width;
    private int height;
    private int marginLeft = 5;

    @JsonAlias("placement") // To insure API v1/v2 backward compatibility
    private PositionOrigin origin = TOP_RIGHT;

    private String onText;
    private boolean centered = false;

}
