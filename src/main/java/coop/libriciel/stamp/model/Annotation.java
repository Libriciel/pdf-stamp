/*
 * PDF-Stamp
 * Copyright (C) 2017-2024 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.stamp.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.itextpdf.io.image.ImageData;
import com.itextpdf.kernel.geom.Rectangle;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.annot.PdfMarkupAnnotation;
import com.itextpdf.kernel.pdf.xobject.PdfFormXObject;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;


@Data
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public abstract class Annotation {


    protected int page = -1;

    @jakarta.validation.constraints.NotNull
    protected Position position;

    protected float opacity;


    // <editor-fold desc="Visitor pattern">


    public abstract float getWidth(@NotNull AnnotationVisitor v, @Nullable ImageData imageData);


    public abstract float getHeight(@NotNull AnnotationVisitor v, @Nullable ImageData imageData);


    public abstract @NotNull PdfMarkupAnnotation generatePdfMarkupAnnotation(@NotNull AnnotationVisitor v, @NotNull Rectangle rectangle);


    public abstract @NotNull PdfFormXObject createPdfFormXObject(@NotNull AnnotationVisitor v, @NotNull PdfDocument document);


    // </editor-fold desc="Visitor pattern">

}
