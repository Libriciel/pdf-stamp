/*
 * PDF-Stamp
 * Copyright (C) 2017-2024 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.stamp.model;

import com.itextpdf.io.image.ImageData;
import com.itextpdf.kernel.geom.Rectangle;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.annot.PdfMarkupAnnotation;
import com.itextpdf.kernel.pdf.xobject.PdfFormXObject;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;


public interface AnnotationVisitor {


    float getWidth(@NotNull Stamp stamp, @Nullable ImageData imageData);

    float getWidth(@NotNull Comment comment, @Nullable ImageData imageData);

    float getHeight(@NotNull Stamp stamp, @Nullable ImageData imageData);

    float getHeight(@NotNull Comment comment, @Nullable ImageData imageData);

    @NotNull PdfMarkupAnnotation generatePdfMarkupAnnotation(@NotNull Stamp stamp, Rectangle position);

    @NotNull PdfMarkupAnnotation generatePdfMarkupAnnotation(@NotNull Comment comment, Rectangle position);

    @NotNull PdfFormXObject createPdfFormXObject(@NotNull PdfDocument document, @NotNull Stamp stamp);

    @NotNull PdfFormXObject createPdfFormXObject(@NotNull PdfDocument document, @NotNull Comment comment);


}
