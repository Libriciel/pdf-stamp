/*
 * PDF-Stamp
 * Copyright (C) 2017-2024 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.stamp.controller;

import com.itextpdf.kernel.pdf.PdfWriter;
import coop.libriciel.stamp.error.RestInvalidPdfException;
import coop.libriciel.stamp.model.request.StampRequest;
import coop.libriciel.stamp.service.HttpResponseUtils;
import coop.libriciel.stamp.service.StampService;
import coop.libriciel.stamp.util.ApiUtils;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.servlet.http.HttpServletResponse;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.OutputStream;
import java.util.List;

import static coop.libriciel.stamp.PdfStampApplication.API_V3;
import static coop.libriciel.stamp.util.ApiUtils.*;
import static org.springframework.http.HttpHeaders.CONTENT_DISPOSITION;
import static org.springframework.http.HttpStatus.OK;
import static org.springframework.http.MediaType.APPLICATION_PDF_VALUE;
import static org.springframework.http.MediaType.MULTIPART_FORM_DATA_VALUE;


@Log4j2
@RestController
@RequestMapping(value = API_V3)
@Tag(name = "stamp", description = "Stamp operations on PDF")
public class StampController {


    // <editor-fold desc="Bean">


    private final StampService stampService;


    @Autowired
    public StampController(StampService stampService) {
        this.stampService = stampService;
    }


    // </editor-fold desc="Bean">


    @PostMapping(path = "stamp/add", consumes = MULTIPART_FORM_DATA_VALUE)
    @ResponseStatus(OK)
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200, content = @Content(
                    mediaType = APPLICATION_PDF_VALUE,
                    schema = @Schema(type = SCHEMA_TYPE_STRING, format = SCHEMA_FORMAT_BINARY)
            )),
            @ApiResponse(responseCode = CODE_400, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public void addStamp(@RequestPart MultipartFile pdfSource,
                         @RequestPart(required = false) List<MultipartFile> images,
                         @RequestPart StampRequest stampRequest,
                         HttpServletResponse response) throws IOException, RestInvalidPdfException {

        log.debug("addStampAnnotation fileSize:{} images:{} stamps:{}", pdfSource.getSize(), CollectionUtils.size(images), stampRequest.getStampList().size());
        stampRequest.getStampList().forEach(stamp -> ApiUtils.streamImageInPlace(stamp, images));

        try (OutputStream outputStream = response.getOutputStream();
             PdfWriter writer = new PdfWriter(outputStream)) {

            HttpResponseUtils.handleFile(
                    pdfSource,
                    true,
                    true,
                    document -> stampService.addAnnotation(document, stampRequest.getStampList()),
                    writer
            );

            response.setContentType(APPLICATION_PDF_VALUE);
            response.setHeader(CONTENT_DISPOSITION, "attachment;filename=%s".formatted(pdfSource.getOriginalFilename()));
        }
    }


}
