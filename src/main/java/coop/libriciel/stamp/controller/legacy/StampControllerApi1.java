/*
 * PDF-Stamp
 * Copyright (C) 2017-2024 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.stamp.controller.legacy;

import com.google.gson.Gson;
import coop.libriciel.stamp.error.RestInvalidPdfException;
import coop.libriciel.stamp.model.Stamp;
import coop.libriciel.stamp.model.request.StampRequest;
import coop.libriciel.stamp.service.HttpResponseUtils;
import coop.libriciel.stamp.service.StampService;
import coop.libriciel.stamp.util.ApiUtils;
import coop.libriciel.stamp.util.DocumentUtils;
import jakarta.validation.Valid;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.beans.PropertyEditorSupport;
import java.io.IOException;

import static java.util.Collections.singletonList;


@Log4j2
@Validated
@Deprecated
@RestController
public class StampControllerApi1 {


    private final StampService stampService;


    @Autowired
    public StampControllerApi1(StampService stampService) {
        this.stampService = stampService;
    }


    /**
     * Define the binder manually, because spring cannot handle multipart and object
     */
    @InitBinder
    public void initBinder(WebDataBinder dataBinder) {

        dataBinder.registerCustomEditor(Stamp.class, new PropertyEditorSupport() {
            Object value;


            @Override
            public Object getValue() {
                return value;
            }


            @Override
            public void setAsText(String text) {
                value = new Gson().fromJson(text, Stamp.class);
            }
        });

        dataBinder.registerCustomEditor(StampRequest.class, new PropertyEditorSupport() {
            Object value;


            @Override
            public Object getValue() {
                return value;
            }


            @Override
            public void setAsText(String text) {
                value = new Gson().fromJson(text, StampRequest.class);
            }
        });

    }


    @PostMapping(path = "/multiple")
    public ResponseEntity<InputStreamResource> stamp(@RequestParam("file") MultipartFile file,
                                                     @RequestParam("metadata") @Valid StampRequest stamps) throws IOException, RestInvalidPdfException {
        // Legacy fixes

        stamps.getStampList().forEach(ApiUtils::streamImageInPlace);
        stamps.getStampList().forEach(DocumentUtils::fixAnnotationPageBeforeApi3);

        // Actual stamp

        return HttpResponseUtils.handleFile(
                file,
                false,
                true,
                document -> stampService.addAnnotation(document, stamps.getStampList())
        );
    }


    @PostMapping(path = "/")
    public ResponseEntity<InputStreamResource> stamp(@RequestParam("file") MultipartFile file,
                                                     @RequestParam("metadata") @Valid Stamp stamp) throws IOException, RestInvalidPdfException {

        // Legacy fixes

        ApiUtils.streamImageInPlace(stamp);
        DocumentUtils.fixAnnotationPageBeforeApi3(stamp);

        // Actual stamp

        return HttpResponseUtils.handleFile(
                file,
                false,
                true,
                document -> stampService.addAnnotation(document, singletonList(stamp))
        );
    }


}
