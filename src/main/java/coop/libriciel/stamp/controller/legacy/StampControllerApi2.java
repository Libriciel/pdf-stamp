/*
 * PDF-Stamp
 * Copyright (C) 2017-2024 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.stamp.controller.legacy;

import com.google.gson.Gson;
import coop.libriciel.stamp.error.RestInvalidPdfException;
import coop.libriciel.stamp.model.Comment;
import coop.libriciel.stamp.model.Stamp;
import coop.libriciel.stamp.model.request.StampRequest;
import coop.libriciel.stamp.service.HttpResponseUtils;
import coop.libriciel.stamp.service.StampService;
import coop.libriciel.stamp.util.ApiUtils;
import coop.libriciel.stamp.util.DocumentUtils;
import jakarta.validation.Valid;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.beans.PropertyEditorSupport;
import java.io.IOException;

import static coop.libriciel.stamp.PdfStampApplication.API_V2;
import static java.util.Collections.singletonList;


@Log4j2
@Validated
@Deprecated
@RestController("annotation")
@RequestMapping(value = API_V2)
public class StampControllerApi2 {


    // <editor-fold desc="Bean">


    private final StampService stampService;


    @Autowired
    public StampControllerApi2(StampService stampService) {
        this.stampService = stampService;
    }


    /**
     * Define the binder manually, because spring cannot handle multipart and object
     */
    @InitBinder
    public void initBinder(WebDataBinder dataBinder) {

        dataBinder.registerCustomEditor(Stamp.class, new PropertyEditorSupport() {

            Object value;


            @Override
            public Object getValue() {
                return value;
            }


            @Override
            public void setAsText(String text) {
                value = new Gson().fromJson(text, Stamp.class);
            }

        });

        dataBinder.registerCustomEditor(Comment.class, new PropertyEditorSupport() {

            Object value;


            @Override
            public Object getValue() {
                return value;
            }


            @Override
            public void setAsText(String text) {
                value = new Gson().fromJson(text, Comment.class);
            }

        });
    }


    // </editor-fold desc="Bean">


    @PostMapping(path = "add/stamp")
    public ResponseEntity<InputStreamResource> addStampAnnotation(@RequestPart("file") MultipartFile file,
                                                                  @RequestPart("request") @Valid StampRequest stampRequest) throws IOException, RestInvalidPdfException {

        log.info("addStampAnnotation file:{} stampRequest:{}", file.getSize(), stampRequest);

        // Legacy fixes

        stampRequest.getStampList().forEach(ApiUtils::streamImageInPlace);
        stampRequest.getStampList().forEach(DocumentUtils::fixAnnotationPageBeforeApi3);

        // Actual stamp

        return HttpResponseUtils.handleFile(
                file,
                true,
                true,
                (document) -> stampService.addAnnotation(document, stampRequest.getStampList())
        );
    }


    @PostMapping(path = "add/text")
    public ResponseEntity<InputStreamResource> addTextAnnotation(@RequestParam("file") MultipartFile file,
                                                                 @RequestParam("metadata") Comment comment) throws IOException, RestInvalidPdfException {
        log.info("addTextAnnotation");

        // Legacy fixes

        ApiUtils.streamImageInPlace(comment);
        DocumentUtils.fixAnnotationPageBeforeApi3(comment);

        // Actual stamp

        return HttpResponseUtils.handleFile(
                file,
                false,
                true,
                document -> stampService.addAnnotation(document, singletonList(comment))
        );
    }


    @PostMapping(path = "delete/text/{commentId}")
    public ResponseEntity<InputStreamResource> removeTextAnnotation(@RequestParam("file") MultipartFile file,
                                                                    @PathVariable("commentId") String commentId) throws IOException, RestInvalidPdfException {

        log.info("removeTextAnnotation id:{}...", commentId);

        return HttpResponseUtils.handleFile(
                file,
                false,
                false,
                document -> stampService.deleteAnnotation(document, commentId)
        );
    }


}
