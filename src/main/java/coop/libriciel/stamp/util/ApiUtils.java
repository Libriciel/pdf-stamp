/*
 * PDF-Stamp
 * Copyright (C) 2017-2024 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.stamp.util;


import coop.libriciel.stamp.model.Comment;
import coop.libriciel.stamp.model.Row;
import coop.libriciel.stamp.model.Stamp;
import lombok.Data;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Base64;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import static java.util.Collections.emptyList;
import static org.springframework.http.HttpStatus.NOT_FOUND;


@Log4j2
public class ApiUtils {


    /**
     * This is only used on error declaration.
     * TODO: Find the SpringBoot inner error message model instead.
     */
    @Data
    public static class ErrorResponse {

        private String timestamp;
        private int status;
        private String error;
        private String message;
        private String path;

    }


    public static final String SCHEMA_TYPE_STRING = "string";
    public static final String SCHEMA_FORMAT_BINARY = "binary";

    public static final String CODE_200 = "200";
    public static final String CODE_400 = "400";
    public static final String CODE_404 = "404";
    public static final String CODE_406 = "406";
    public static final String CODE_409 = "409";
    public static final String CODE_410 = "410";
    public static final String CODE_413 = "413";
    public static final String CODE_415 = "415";
    public static final String CODE_500 = "500";
    public static final String CODE_507 = "507";


    /**
     * Insures API v1/v2 backward compatibility
     *
     * @param stamp the legacy stamp
     */
    @Deprecated
    public static void streamImageInPlace(@NotNull Stamp stamp) {
        Optional.ofNullable(stamp.getRows())
                .orElse(emptyList())
                .stream()
                .map(Row::getLogo)
                .filter(Objects::nonNull)
                .filter(logo -> StringUtils.isNotEmpty(logo.getData()))
                .forEach(logo -> {
                    byte[] logoBytes = Base64.getDecoder().decode(logo.getData());
                    logo.setImageData(new ByteArrayInputStream(logoBytes));
                });
    }


    /**
     * Insures API v1/v2 backward compatibility
     *
     * @param comment the legacy stamp
     */
    @Deprecated
    public static void streamImageInPlace(@NotNull Comment comment) {
        byte[] logoBytes = Base64.getDecoder().decode(comment.getIconBase64());
        comment.setImageData(new ByteArrayInputStream(logoBytes));
    }


    public static void streamImageInPlace(@NotNull Stamp stamp, List<MultipartFile> images) {

        stamp.getRows().stream()
                .map(Row::getLogo)
                .filter(Objects::nonNull)
                .filter(logo -> StringUtils.isNotEmpty(logo.getImageRef()))
                .forEach(logo -> {

                    InputStream targetImageStream = images.stream()
                            .filter(imageFile -> StringUtils.equals(logo.getImageRef(), imageFile.getOriginalFilename()))
                            .findFirst()
                            .map(imageFile -> {
                                try {
                                    return imageFile.getInputStream();
                                } catch (IOException e) {
                                    log.error("Error opening image", e);
                                    return null;
                                }
                            })
                            .orElseThrow(() -> new ResponseStatusException(NOT_FOUND, "Cannot find the image reference"));

                    log.debug("Found image {} in multipart files", logo.getImageRef());
                    logo.setImageData(targetImageStream);
                });
    }


    public static void streamImageInPlace(@NotNull Comment comment, Resource commentIconResource, Resource signaturePlacementIconResource) {

        Resource iconResource = switch (comment.getIcon()) {
            case SIGNATURE_PLACEMENT -> signaturePlacementIconResource;
            case STICKY_NOTE -> commentIconResource;
        };

        try {
            comment.setImageData(iconResource.getInputStream());
        } catch (IOException e) {
            log.error("Cannot read standard comment icon");
            throw new RuntimeException(e);
        }
    }


}
