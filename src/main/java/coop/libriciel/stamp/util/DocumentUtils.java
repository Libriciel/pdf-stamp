/*
 * PDF-Stamp
 * Copyright (C) 2017-2024 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.stamp.util;

import coop.libriciel.stamp.model.Annotation;
import org.jetbrains.annotations.Nullable;


public class DocumentUtils {


    /**
     * Compute the page, if exists :
     * * 1 : first page
     * * 2 : second page
     * <p>
     * Some default case:
     * * 0 : every page
     * <p>
     * Counting from the end:
     * * -1 : last page
     * * -2 : second to last page
     *
     * @param numberOfPages the real PDF count
     * @param page          the asked one
     * @return the computed one
     */
    public static @Nullable Integer computePdfPageNumber(int numberOfPages, int page) {

        boolean positiveOverflow = page > numberOfPages;
        boolean negativeOverflow = (-page) > numberOfPages;
        if (positiveOverflow || negativeOverflow) {
            return null;
        }

        if (page < 0) {
            return numberOfPages + 1 + page;
        }

        return page;
    }


    /**
     * Prior to API3, the paging system was:
     * * 0 : last page
     * * -1 : last page
     * <p>
     * The negative ones (-2, etc) did not exist.
     *
     * @param annotation the legacy-paged one
     */
    @Deprecated
    public static void fixAnnotationPageBeforeApi3(Annotation annotation) {
        int legacyPage = annotation.getPage();
        annotation.setPage(switch (legacyPage) {
            case -1 -> 0;
            case 0 -> -1;
            default -> legacyPage;
        });
    }


}
