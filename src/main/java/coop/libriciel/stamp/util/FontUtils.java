/*
 * PDF-Stamp
 * Copyright (C) 2017-2024 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.stamp.util;

import com.itextpdf.kernel.font.PdfFont;
import com.itextpdf.kernel.font.PdfFontFactory;

import java.io.IOException;

import static com.itextpdf.io.font.PdfEncodings.CP1252;
import static com.itextpdf.io.font.constants.StandardFonts.HELVETICA;
import static com.itextpdf.kernel.font.PdfFontFactory.EmbeddingStrategy.PREFER_NOT_EMBEDDED;


public class FontUtils {

    private static final int SPACE_BETWEEN_LINES = 6;


    public static float getFontWidth(String text, int size) {
        try {
            PdfFont font = PdfFontFactory.createFont(HELVETICA, CP1252, PREFER_NOT_EMBEDDED);
            return font.getWidth(text, size);
        } catch (IOException e) {
            throw new RuntimeException("Missing helvetica font", e);
        }
    }


    public static float getFontHeight(int size) {
        try {
            PdfFont font = PdfFontFactory.createFont(HELVETICA, CP1252, PREFER_NOT_EMBEDDED);
            float ascent = font.getAscent("ABCDEF", size);
            float descent = font.getDescent("ABCDEF", size);
            return ascent - descent;
        } catch (IOException e) {
            throw new RuntimeException("Missing helvetica font", e);
        }
    }


    public static float getMaxHeight(int fontSize, int totalRow, int row, int height, float spaces) {
        float fontHeight = getFontHeight(fontSize);
        float spacesBetweenLines = SPACE_BETWEEN_LINES;
        if (spaces != 0) {
            spacesBetweenLines = spaces;
        } else if (height != 0) {
            spacesBetweenLines = (height - totalRow * fontHeight - 3) / totalRow;
        }
        return 4 + ((totalRow - row) * (fontHeight + spacesBetweenLines));
    }


}
