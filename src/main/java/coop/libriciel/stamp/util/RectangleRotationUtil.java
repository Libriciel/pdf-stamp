/*
 * PDF-Stamp
 * Copyright (C) 2017-2024 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.stamp.util;

import com.itextpdf.kernel.geom.Rectangle;
import coop.libriciel.stamp.model.PositionOrigin;

import static coop.libriciel.stamp.model.PositionOrigin.TOP_RIGHT;


public class RectangleRotationUtil {


    private RectangleRotationUtil() {
    }


    public static Rectangle rotateRectangle(Rectangle rectangle, Rectangle pageSize, int rotation, PositionOrigin placement) {
        return (placement == TOP_RIGHT)
                ? performTopRightPlacement(rectangle, pageSize, rotation)
                : performBottomLeftPlacement(rectangle, pageSize, rotation);
    }


    /**
     * Bottom left placement is placement by default in PDF files
     *
     * @param rectangle The rectangle to place
     * @param pageSize  The page size
     * @param rotation  The rotation of the page
     * @return A new position with required rotation
     */
    private static Rectangle performBottomLeftPlacement(Rectangle rectangle, Rectangle pageSize, int rotation) {
        return switch (rotation) {
            case 90 -> return90Rectangle(rectangle, pageSize);
            case 180 -> return180Rectangle(rectangle, pageSize);
            case 270 -> return270Rectangle(rectangle, pageSize);
            default -> returnOriginalRectangle(rectangle, pageSize);
        };
    }


    /**
     * Top right placement inverse 0 / 180 and 90 / 270 rotations
     */
    private static Rectangle performTopRightPlacement(Rectangle rectangle, Rectangle pageSize, int rotation) {
        return switch (rotation) {
            case 90 -> return270Rectangle(rectangle, pageSize);
            case 180 -> returnOriginalRectangle(rectangle, pageSize);
            case 270 -> return90Rectangle(rectangle, pageSize);
            default -> return180Rectangle(rectangle, pageSize);
        };
    }


    private static Rectangle returnOriginalRectangle(Rectangle rectangle, Rectangle pageSize) {
        return new Rectangle(
                pageSize.getX() + rectangle.getX(),
                pageSize.getY() + rectangle.getY(),
                rectangle.getWidth(),
                rectangle.getHeight());
    }


    private static Rectangle return180Rectangle(Rectangle rectangle, Rectangle pageSize) {
        return new Rectangle(
                pageSize.getX() + pageSize.getWidth() - rectangle.getWidth() - rectangle.getX(),
                pageSize.getY() + pageSize.getHeight() - rectangle.getHeight() - rectangle.getY(),
                rectangle.getWidth(),
                rectangle.getHeight());
    }


    private static Rectangle return90Rectangle(Rectangle rectangle, Rectangle pageSize) {
        return new Rectangle(
                pageSize.getX() + pageSize.getWidth() - rectangle.getHeight() - rectangle.getY(),
                pageSize.getY() + rectangle.getX(),
                rectangle.getHeight(),
                rectangle.getWidth());
    }


    private static Rectangle return270Rectangle(Rectangle rectangle, Rectangle pageSize) {
        return new Rectangle(
                pageSize.getX() + rectangle.getY(),
                pageSize.getY() + pageSize.getHeight() - rectangle.getWidth() - rectangle.getX(),
                rectangle.getHeight(),
                rectangle.getWidth());
    }


}
