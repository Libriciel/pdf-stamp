/*
 * PDF-Stamp
 * Copyright (C) 2017-2024 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.stamp.util;

import com.itextpdf.kernel.geom.Vector;
import com.itextpdf.kernel.pdf.canvas.parser.EventType;
import com.itextpdf.kernel.pdf.canvas.parser.data.IEventData;
import com.itextpdf.kernel.pdf.canvas.parser.data.TextRenderInfo;
import com.itextpdf.kernel.pdf.canvas.parser.listener.IEventListener;

import java.util.Collections;
import java.util.List;
import java.util.Set;

import static com.itextpdf.kernel.pdf.canvas.parser.EventType.RENDER_TEXT;

/**
 * Un TextRenderListener 'custom' pour detecter et localiser une chaine de
 * caracteres dans une page PDF.
 * Merci iText...
 *
 * @author Stephane Vast, Lukas Hameury
 */
public class TextLocatorListener implements IEventListener {

    private String signatureTag;
    private List<Vector> out;
    private Vector startPoint;
    private StringBuilder builder;
    private boolean isFinding;

    public TextLocatorListener(String signatureTagString, List<Vector> outList) {
        this.signatureTag = signatureTagString;
        this.out = outList;
        this.startPoint = new Vector(0, 0, 1);
        this.isFinding = false;
        this.builder = new StringBuilder();
    }

    private void renderText(TextRenderInfo renderInfo) {
        if (!isFinding && renderInfo.getText().contains(signatureTag.substring(0, 1))) {
            isFinding = true;
            builder.setLength(0);
            builder.append(renderInfo.getText().replaceAll("\\s", ""));
            startPoint = renderInfo.getAscentLine().getStartPoint();
            // Sometimes, we get all signature tag in one shot... Sometimes not !
            if (renderInfo.getText().equals(signatureTag)) {
                out.add(pointMilieu(startPoint, renderInfo.getBaseline().getEndPoint()));
                isFinding = false;
            }
        } else if (isFinding) {
            builder.append(renderInfo.getText().replaceAll("\\s", ""));
            if (signatureTag.length() <= builder.length()) {
                if (builder.toString().contains(signatureTag)) {
                    out.add(pointMilieu(startPoint, renderInfo.getBaseline().getEndPoint()));
                }
                // on a dépassé le curseur
                isFinding = false;
            } else if (!signatureTag.contains(builder)) {
                isFinding = false;
                renderText(renderInfo);
            }
        }
    }

    private Vector pointMilieu(Vector start, Vector end) {
        float x = (start.get(Vector.I1) + end.get(Vector.I1)) / 2;
        float y = (start.get(Vector.I2) + end.get(Vector.I2)) / 2;
        float z = (start.get(Vector.I3) + end.get(Vector.I3)) / 2;
        return new Vector(x, y, z);
    }

    @Override
    public void eventOccurred(IEventData data, EventType type) {
        if (type.equals(RENDER_TEXT) && data instanceof TextRenderInfo) {
            renderText((TextRenderInfo) data);
        }
    }

    @Override
    public Set<EventType> getSupportedEvents() {
        return Collections.singleton(RENDER_TEXT);
    }
}
