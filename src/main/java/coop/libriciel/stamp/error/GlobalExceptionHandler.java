/*
 * PDF-Stamp
 * Copyright (C) 2017-2024 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.stamp.error;

import com.google.gson.stream.MalformedJsonException;
import jakarta.annotation.Priority;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.validation.ConstraintViolationException;
import org.apache.catalina.connector.ClientAbortException;
import org.hibernate.validator.internal.engine.path.PathImpl;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.multipart.MultipartException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.Collections;
import java.util.Map;
import java.util.stream.Collectors;


@ControllerAdvice
@Priority(1)
@Order(1)
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {


    @ExceptionHandler
    @ResponseBody
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public Map handle(ConstraintViolationException exception) {
        return error(exception.getConstraintViolations()
                .stream()
                .map(n -> ((PathImpl) n.getPropertyPath()).getLeafNode().asString() + " " + n.getMessage())
                .collect(Collectors.toList()));
    }


    @ExceptionHandler
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    public void handleMalformedJSON(MalformedJsonException ex) {
        logger.error("MalformedJsonException : " + ex.getMessage());
    }


    @ExceptionHandler(MultipartException.class)
    @ResponseStatus(value = HttpStatus.PAYLOAD_TOO_LARGE)
    public void handleMultipartException(MultipartException ex) {
        logger.error(ex.getMessage());
    }


    @ExceptionHandler(ClientAbortException.class)
    @ResponseStatus(HttpStatus.GONE)
    public void abortHandler(ClientAbortException e, HttpServletRequest req) {
        logger.warn("Client abort exception catched on :" + req.getRequestURL());
    }


    private Map error(Object message) {
        return Collections.singletonMap("error", message);
    }


}
