/*
 * PDF-Stamp
 * Copyright (C) 2017-2024 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.stamp;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Collections;
import java.util.Map;

import static coop.libriciel.stamp.utils.IntegrationTestUtils.*;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;

@ExtendWith(SpringExtension.class)
@AutoConfigureMockMvc(addFilters = false)
@SpringBootTest(webEnvironment = RANDOM_PORT)
public class IP4IntegrationTest {

    @Autowired
    private MockMvc mockMvc;
    private final ClassLoader classLoader = getClass().getClassLoader();


    @Test
    void addStamp_apiV2() throws Exception {
        String apiTargetPath = "/v2/add/stamp";
        String producedTestFileSuffix = "_addStamp_apiV2.pdf";
        callApiV2WithTestData(mockMvc, getClass(), ip4MultiV2JsonPath, apiTargetPath, producedTestFileSuffix);
    }
    @Test
    void addStamp_apiV3() throws Exception {
        String apiTargetPath = "/v3/stamp/add";
        String producedTestFileSuffix = "_addStamp_apiV3.pdf";
        Map<String, String> base64ImagesMap = Collections.singletonMap(sampleApiv3ImgRef, buildImageBase64(classLoader, BIG_LOGO_PNG_FILENAME));
        callApiV3WithTestData(mockMvc, getClass(), ip4MultiV3JsonPath, apiTargetPath, producedTestFileSuffix, base64ImagesMap);
    }



}
