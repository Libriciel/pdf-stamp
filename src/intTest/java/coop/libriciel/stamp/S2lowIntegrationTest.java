/*
 * PDF-Stamp
 * Copyright (C) 2017-2024 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package coop.libriciel.stamp;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Map;

import static coop.libriciel.stamp.utils.IntegrationTestUtils.*;
import static org.apache.commons.io.FileUtils.writeByteArrayToFile;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.multipart;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@ExtendWith(SpringExtension.class)
@AutoConfigureMockMvc(addFilters = false)
@SpringBootTest(webEnvironment = RANDOM_PORT)
public class S2lowIntegrationTest {

    @Autowired private MockMvc mockMvc;
    private final ClassLoader classLoader = getClass().getClassLoader();

    private final File sampleJson = new File("src/intTest/resources/sample-s2low-topRight.json");
    private final File multipleSampleJson = new File("src/intTest/resources/multiple-sample-s2low-topRight.json");

    private MockMultipartFile minimalPdfMultipartFile;


    @BeforeEach
    void prepare() throws IOException {
        this.minimalPdfMultipartFile = new MockMultipartFile(
                "file",
                "filename.pdf",
                "application/pdf",
                Files.readAllBytes(new File("src/test/resources/minimal.pdf").toPath()));
    }


    @Test
    void whenNoFile_thenStatus400() throws Exception {
        this.mockMvc
                .perform(multipart("/"))
                .andExpect(status().isBadRequest());
    }


    @Test
    void whenFileNoPDF_thenStatus422() throws Exception {

        MockMultipartFile file = new MockMultipartFile(
                "file",
                "filename.txt",
                "text/plain",
                "some text".getBytes()
        );

        this.mockMvc
                .perform(
                        multipart("/")
                                .file(file)
                                .param("metadata", new String(Files.readAllBytes(sampleJson.toPath())))
                )
                .andExpect(status().isUnprocessableEntity());
    }


    @Test
    void whenFileEmptyPDF_thenStatus422() throws Exception {

        MockMultipartFile file = new MockMultipartFile(
                "file",
                "filename.pdf",
                "application/pdf",
                "some text".getBytes()
        );

        this.mockMvc
                .perform(
                        multipart("/")
                                .file(file)
                                .param("metadata", new String(Files.readAllBytes(sampleJson.toPath()))))
                .andExpect(status().isUnprocessableEntity());
    }


    @Test
    void whenStampNoFile_thenStatus400() throws Exception {
        this.mockMvc
                .perform(
                        multipart("/")
                                .param("metadata", new String(Files.readAllBytes(sampleJson.toPath()))))
                .andExpect(status().isBadRequest());
    }


    @Test
    void whenFilePDFNoStamp_thenStatus400() throws Exception {

        this.mockMvc
                .perform(
                        multipart("/").file(minimalPdfMultipartFile)
                )
                .andExpect(status().isBadRequest());
    }


    @Test
    void whenFilePDFEmptyStamp_thenStatus400() throws Exception {
        this.mockMvc
                .perform(
                        multipart("/")
                                .file(minimalPdfMultipartFile)
                                .param("metadata", "{}")
                )
                .andExpect(status().isBadRequest());
    }


    @Test
    void whenFilePDFStamp_thenStatus200() throws Exception {

        String sampleJsonString = new String(Files.readAllBytes(sampleJson.toPath()));

        // The standard MockMvc#print() method will print the entire response body.
        // When the response is a PDF file, we don't want that.
        // So we fake the logs here :
        System.out.println();
        System.out.println("MockHttpServletRequest:");
        System.out.println("     Request URI = /");
        System.out.println("     Method      = POST");
        System.out.println("     Parameters  = " + sampleJsonString);

        this.mockMvc
                .perform(
                        multipart("/")
                                .file(minimalPdfMultipartFile)
                                .param("metadata", sampleJsonString)
                )
                .andExpect(status().isOk())
                .andDo(result -> {
                    byte[] finalSignedFileBytes = result.getResponse().getContentAsByteArray();
                    assertNotEquals(0, finalSignedFileBytes.length);
                    String signatureResult = "build/test-results/intTest_" + getClass().getSimpleName() + "_whenFilePDFStamp.pdf";
                    File signedFile = new File(signatureResult);
                    writeByteArrayToFile(signedFile, finalSignedFileBytes);
                });
    }


    @Test
    void whenFilePDFMultipleStamp_thenStatus200() throws Exception {

        String sampleJsonString = new String(Files.readAllBytes(multipleSampleJson.toPath()));

        // The standard MockMvc#print() method will print the entire response body.
        // When the response is a PDF file, we don't want that.
        // So we fake the logs here :
        System.out.println();
        System.out.println("MockHttpServletRequest:");
        System.out.println("     Request URI = /multiple");
        System.out.println("     Method      = POST");
        System.out.println("     Parameters  = " + sampleJsonString);

        this.mockMvc
                .perform(
                        multipart("/multiple")
                                .file(minimalPdfMultipartFile)
                                .param("metadata", sampleJsonString)
                )
                .andExpect(status().isOk())
                .andDo(result -> {
                    byte[] finalSignedFileBytes = result.getResponse().getContentAsByteArray();
                    assertNotEquals(0, finalSignedFileBytes.length);
                    String signatureResult = "build/test-results/intTest_" + getClass().getSimpleName() + "_whenFilePDFMultipleStamp.pdf";
                    File signedFile = new File(signatureResult);
                    writeByteArrayToFile(signedFile, finalSignedFileBytes);
                });
    }


    @Test
    void whenFilePDFInvalid_thenStatus200() throws Exception {

        String sampleJsonString = new String(Files.readAllBytes(sampleJson.toPath()));

        MockMultipartFile file = new MockMultipartFile(
                "file",
                "filename.pdf",
                "application/pdf",
                Files.readAllBytes(new File("src/test/resources/invalid.pdf").toPath()));

        // The standard MockMvc#print() method will print the entire response body.
        // When the response is a PDF file, we don't want that.
        // So we fake the logs here :
        System.out.println();
        System.out.println("MockHttpServletRequest:");
        System.out.println("     Request URI = /");
        System.out.println("     Method      = POST");
        System.out.println("     Parameters  = " + sampleJsonString);

        this.mockMvc
                .perform(
                        multipart("/")
                                .file(file)
                                .param("metadata", sampleJsonString)
                )
                .andExpect(status().isOk())
                .andDo(result -> {
                    byte[] finalSignedFileBytes = result.getResponse().getContentAsByteArray();
                    assertNotEquals(0, finalSignedFileBytes.length);
                    String signatureResult = "build/test-results/intTest_" + getClass().getSimpleName() + "_whenFilePDFInvalid.pdf";
                    File signedFile = new File(signatureResult);
                    writeByteArrayToFile(signedFile, finalSignedFileBytes);
                });
    }


    @Test
    void whenProtectedFilePDFStamp_thenStatus200() throws Exception {

        String sampleJsonString = new String(Files.readAllBytes(sampleJson.toPath()));

        MockMultipartFile file = new MockMultipartFile(
                "file",
                "filename.pdf",
                "application/pdf",
                Files.readAllBytes(new File("src/test/resources/password-protected.pdf").toPath())
        );

        // The standard MockMvc#print() method will print the entire response body.
        // When the response is a PDF file, we don't want that.
        // So we fake the logs here :
        System.out.println();
        System.out.println("MockHttpServletRequest:");
        System.out.println("     Request URI = /");
        System.out.println("     Method      = POST");
        System.out.println("     Parameters  = " + sampleJsonString);

        this.mockMvc
                .perform(
                        multipart("/")
                                .file(file)
                                .param("metadata", sampleJsonString)
                )
                .andExpect(status().isOk())
                .andDo(result -> {
                    byte[] finalSignedFileBytes = result.getResponse().getContentAsByteArray();
                    assertNotEquals(0, finalSignedFileBytes.length);
                    String signatureResult = "build/test-results/intTest_" + getClass().getSimpleName() + "_whenProtectedFilePDFStamp.pdf";
                    File signedFile = new File(signatureResult);
                    writeByteArrayToFile(signedFile, finalSignedFileBytes);
                });
    }


    @Test
    public void whenFilePDFBroken_thenStatus200() throws Exception {

        String sampleJsonString = new String(Files.readAllBytes(sampleJson.toPath()));

        MockMultipartFile file = new MockMultipartFile(
                "file",
                "filename.pdf",
                "application/pdf",
                Files.readAllBytes(new File("src/test/resources/broken.pdf").toPath()));

        // The standard MockMvc#print() method will print the entire response body.
        // When the response is a PDF file, we don't want that.
        // So we fake the logs here :
        System.out.println();
        System.out.println("MockHttpServletRequest:");
        System.out.println("     Request URI = /");
        System.out.println("     Method      = POST");
        System.out.println("     Parameters  = " + sampleJsonString);

        this.mockMvc
                .perform(
                        multipart("/")
                                .file(file)
                                .param("metadata", sampleJsonString)
                )
                .andExpect(status().isOk())
                .andDo(result -> {
                    byte[] finalSignedFileBytes = result.getResponse().getContentAsByteArray();
                    assertNotEquals(0, finalSignedFileBytes.length);
                    String signatureResult = "build/test-results/intTest_" + getClass().getSimpleName() + "_whenFilePDFBroken.pdf";
                    File signedFile = new File(signatureResult);
                    writeByteArrayToFile(signedFile, finalSignedFileBytes);
                });
    }


    /**
     * Api V2
     */

    @Test
    void addStamp_apiV2_nologo() throws Exception {
        String apiTargetPath = "/v2/add/stamp";
        String producedTestFileSuffix = "_addStamp_apiV2_nologo.pdf";
        callApiV2WithTestData(mockMvc, getClass(), sampleJsonV2NoLogoPath, apiTargetPath, producedTestFileSuffix);
    }


    @Test
    void addStamp_apiV2() throws Exception {
        String apiTargetPath = "/v2/add/stamp";
        String producedTestFileSuffix = "_addStamp_apiV2.pdf";
        callApiV2WithTestData(mockMvc, getClass(), sampleJsonV2Path, apiTargetPath, producedTestFileSuffix);
    }


    /**
     * Api V3
     */

    @Test
    void addStamp_apiV3_nologo() throws Exception {
        String apiTargetPath = "/v3/stamp/add";
        String producedTestFileSuffix = "_addStamp_apiV3_nologo.pdf";
        callApiV3WithTestData(mockMvc, getClass(), sampleJsonV3NoLogoPath, apiTargetPath, producedTestFileSuffix, null);
    }

    @Test
    void addStamp_apiV3_single() throws Exception {
        String apiTargetPath = "/v3/stamp/add";
        String producedTestFileSuffix = "_addStamp_apiV3_single.pdf";
        Map<String, String> base64ImagesMap = Map.of(
                sampleApiv3ImgRef2, buildImageBase64(classLoader, S2LOW_LOGO_PNG_FILENAME)
        );

        callApiV3WithTestData(mockMvc, getClass(), s2lowSingleV3JsonPath, apiTargetPath, producedTestFileSuffix, base64ImagesMap);
    }
    @Test
    void addStamp_apiV3_double_logo() throws Exception {
        String apiTargetPath = "/v3/stamp/add";
        String producedTestFileSuffix = "_addStamp_apiV3_double_logo.pdf";
        Map<String, String> base64ImagesMap = Map.of(
                sampleApiv3ImgRef2, buildImageBase64(classLoader, S2LOW_LOGO_PNG_FILENAME),
                sampleApiv3ImgRef, buildImageBase64(classLoader, SMALL_IP5_LOGO_PNG_FILENAME)
        );

        callApiV3WithTestData(mockMvc, getClass(), s2lowDoubleLogoV3Path, apiTargetPath, producedTestFileSuffix, base64ImagesMap);
    }


}
