/*
 * PDF-Stamp
 * Copyright (C) 2017-2024 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.stamp;

import jakarta.servlet.http.Part;
import org.json.JSONStringer;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockPart;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import java.io.File;
import java.nio.charset.StandardCharsets;
import java.util.Collections;
import java.util.Map;

import static coop.libriciel.stamp.utils.IntegrationTestUtils.*;
import static org.apache.commons.io.FileUtils.writeByteArrayToFile;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;
import static org.springframework.http.MediaType.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.multipart;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@ExtendWith(SpringExtension.class)
@AutoConfigureMockMvc(addFilters = false)
@SpringBootTest(webEnvironment = RANDOM_PORT)
public class IP50IntegrationTest {


    @Autowired private MockMvc mockMvc;
    private final ClassLoader classLoader = getClass().getClassLoader();

    @Test
    void addStamp_apiV3() throws Exception {
        String apiTargetPath = "/v3/stamp/add";
        String producedTestFileSuffix = "_addStamp_apiV3.pdf";
        Map<String, String> base64ImagesMap = Collections.singletonMap(sampleApiv3ImgRef, buildImageBase64(classLoader, BIG_LOGO_PNG_FILENAME));
        callApiV3WithTestData(mockMvc, getClass(), ip5MultiV3JsonPath, apiTargetPath, producedTestFileSuffix, base64ImagesMap);
    }


    @Test
    void addComment_apiV2() throws Exception {

        JSONStringer metadata = new JSONStringer()
                .object()
                /**/.key("page").value(1)
                /**/.key("opacity").value(1)
                /**/.key("name").value("annotation_text_name_01")
                /**/.key("title").value("Adrien")
                /**/.key("contents").value("Lorem ipsum")
                /**/.key("iconBase64").value(buildImageBase64(classLoader, BIG_LOGO_PNG_FILENAME))
                /**/.key("origin").value("TOP_RIGHT")
                /**/.key("rotation").value(0)
                /**/.key("position").object()
                /**//**/.key("width").value(150)
                /**//**/.key("height").value(100)
                /**//**/.key("x").value(200)
                /**//**/.key("y").value(120)
                /**//**/.key("placement").value("TOP_RIGHT")
                /**//**/.key("marginLeft").value(10)
                /**//**/.key("centered").value(true)
                /**/.endObject()
                .endObject();

        String annotationTextDataStr = metadata.toString();

        // The standard MockMvc#print() method will print the entire response body.
        // When the response is a PDF file, we don't want that.
        // So we fake the logs here :
        System.out.println();
        System.out.println("MockHttpServletRequest:");
        System.out.println("     Request URI = /add/text");
        System.out.println("     Method      = POST");
        System.out.println("     Parameters  = " + annotationTextDataStr);

        this.mockMvc
                .perform(
                        multipart("/v2/add/text")
                                .file("file", buildFileContent(classLoader, TEST_PDF_FILENAME))
                                .param("metadata", annotationTextDataStr)
                                .contentType(MULTIPART_FORM_DATA)
                                .accept(APPLICATION_OCTET_STREAM)
                )
                .andExpect(status().isOk())
                .andDo(result -> {
                    byte[] finalSignedFileBytes = result.getResponse().getContentAsByteArray();
                    assertNotEquals(0, finalSignedFileBytes.length);
                    String signatureResult = "build/test-results/intTest_" + getClass().getSimpleName() + "_addComment_apiV2.pdf";
                    File signedFile = new File(signatureResult);
                    writeByteArrayToFile(signedFile, finalSignedFileBytes);
                });
    }


    @Test
    void addComment_apiV3() throws Exception {

        JSONStringer commentReqJsonBuilder = new JSONStringer()
                .object()
                /**/.key("page").value(1)
                /**/.key("opacity").value(1)
                /**/.key("name").value("annotation_text_name_01")
                /**/.key("title").value("Adrien")
                /**/.key("contents").value("Lorem ipsum")
                /**/.key("icon").value(0)
                /**/.key("origin").value("TOP_RIGHT")
                /**/.key("rotation").value(0)
                /**/.key("position").object()
                /**//**/.key("width").value(150)
                /**//**/.key("height").value(100)
                /**//**/.key("x").value(200)
                /**//**/.key("y").value(120)
                /**//**/.key("placement").value("TOP_RIGHT")
                /**//**/.key("marginLeft").value(10)
                /**//**/.key("centered").value(true)
                /**/.endObject()
                .endObject();

        String commentRequestDataStr = commentReqJsonBuilder.toString();

        // The standard MockMvc#print() method will print the entire response body.
        // When the response is a PDF file, we don't want that.
        // So we fake the logs here :
        System.out.println();
        System.out.println("MockHttpServletRequest:");
        System.out.println("     Request URI = /add/text");
        System.out.println("     Method      = POST");
        System.out.println("     Parameters  = " + commentRequestDataStr);


        Part commentRequestPart = new MockPart("comment", null, commentRequestDataStr.getBytes(StandardCharsets.UTF_8), MediaType.APPLICATION_JSON);

        this.mockMvc
                .perform(
                        multipart("/v3/comment/add")
                                .file("pdfSource", buildFileContent(classLoader, TEST_PDF_FILENAME))
                                .part(commentRequestPart)
                                .contentType(MULTIPART_FORM_DATA)
                                .accept(APPLICATION_OCTET_STREAM)
                )
                .andExpect(status().isOk())
                .andDo(result -> {
                    byte[] finalSignedFileBytes = result.getResponse().getContentAsByteArray();
                    assertNotEquals(0, finalSignedFileBytes.length);
                    String signatureResult = "build/test-results/intTest_" + getClass().getSimpleName() + "_addComment_apiV3.pdf";
                    File signedFile = new File(signatureResult);
                    writeByteArrayToFile(signedFile, finalSignedFileBytes);
                });
    }


    @Test
    void removeText_apiV2() throws Exception {

        String url = "/v2/delete/text/annotation_text_name_01";

        // The standard MockMvc#print() method will print the entire response body.
        // When the response is a PDF file, we don't want that.
        // So we fake the logs here :
        System.out.println();
        System.out.println("MockHttpServletRequest:");
        System.out.println("     Request URI = " + url);
        System.out.println("     Method      = POST");

        this.mockMvc
                .perform(
                        multipart(url)
                                .file("file", buildFileContent(classLoader, TEST_PDF_ANNOTATED_FILENAME))
                                .contentType(MULTIPART_FORM_DATA)
                                .accept(APPLICATION_OCTET_STREAM)
                )
                .andExpect(status().isOk())
                .andDo(result -> {
                    byte[] finalSignedFileBytes = result.getResponse().getContentAsByteArray();
                    assertNotEquals(0, finalSignedFileBytes.length);
                    String signatureResult = "build/test-results/intTest_" + getClass().getSimpleName() + "_deleteText.pdf";
                    File signedFile = new File(signatureResult);
                    writeByteArrayToFile(signedFile, finalSignedFileBytes);
                });
    }

}
