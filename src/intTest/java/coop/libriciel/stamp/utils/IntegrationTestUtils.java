/*
 * PDF-Stamp
 * Copyright (C) 2017-2024 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.stamp.utils;

import jakarta.servlet.http.Part;
import org.jetbrains.annotations.NotNull;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.mock.web.MockPart;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMultipartHttpServletRequestBuilder;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Base64;
import java.util.Map;

import static org.apache.commons.io.FileUtils.writeByteArrayToFile;
import static org.apache.commons.io.IOUtils.toByteArray;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.springframework.http.MediaType.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.multipart;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


public class IntegrationTestUtils {

    public static final String sampleApiv3ImgRef = "imageRefOne";
    public static final String sampleApiv3ImgRef2 = "imageRefTwo";

    public static final Path ip4MultiV2JsonPath = Paths.get("src/intTest/resources/ip4-apiv2.json");
    public static final Path ip4MultiV3JsonPath = Paths.get("src/intTest/resources/ip4-apiv3-multi.json");
    public static final Path ip5MultiV3JsonPath = Paths.get("src/intTest/resources/ip5-apiv3-multi.json");
    public static final Path s2lowSingleV3JsonPath = Paths.get("src/intTest/resources/s2low-apiv3-single.json");
    public static final Path s2lowDoubleLogoV3Path = Paths.get("src/intTest/resources/s2low-apiv3-double-logo.json");
    public static final Path sampleJsonV3Path = Paths.get("src/intTest/resources/sample-s2low-apiV3.json");
    public static final Path sampleJsonV3NoLogoPath = Paths.get("src/intTest/resources/sample-s2low-apiV3-nologo.json");
    public static final Path sampleJsonV2NoLogoPath = Paths.get("src/intTest/resources/sample-s2low-apiV2-nologo.json");
    public static final Path sampleJsonV2Path = Paths.get("src/intTest/resources/sample-s2low-apiV2.json");
    public static final Path sampleJsonV1Path = Paths.get("src/intTest/resources/sample-s2low-topRight.json");
    public static final String TEST_PDF_FILENAME = "test.pdf";
    public static final String TEST_PDF_ANNOTATED_FILENAME = "test_annotated.pdf";

    public static final String S2LOW_LOGO_PNG_FILENAME = "s2lowLogo2.png";
    public static final String BIG_LOGO_PNG_FILENAME = "logo.png";
    public static final String SMALL_IP5_LOGO_PNG_FILENAME = "ip-favicon.png";


    public static byte[] buildFileContent(@NotNull ClassLoader classLoader, String resourceName) throws IOException {
        InputStream fileInputStream = classLoader.getResourceAsStream(resourceName);
        assertNotNull(fileInputStream);
        return toByteArray(fileInputStream);
    }


    public static String buildImageBase64(@NotNull ClassLoader classLoader, String imageFileName) throws IOException {
        InputStream fileInputStream = classLoader.getResourceAsStream(imageFileName);
        assertNotNull(fileInputStream);
        return Base64.getEncoder().encodeToString(toByteArray(fileInputStream));
    }

    public static void callApiV3WithTestData(MockMvc mockMvc, Class<?> callerClass, Path jsonPath, String apiTargetPath, String producedTestFileSuffix, Map<String, String> base64ImagesMap) throws Exception {
        byte[] stampRequestContent = Files.readAllBytes(jsonPath);
        String stampRequestContentStr = new String(stampRequestContent);

        // The standard MockMvc#print() method will print the entire response body.
        // When the response is a PDF file, we don't want that.
        // So we fake the logs here :
        System.out.println();
        System.out.println("MockHttpServletRequest:");
        System.out.println("     Request URI = " + apiTargetPath);
        System.out.println("     Method      = POST");
        System.out.println("     Parameters  = " + stampRequestContentStr);

        Part stampRequestPart = new MockPart("stampRequest", null, stampRequestContent, MediaType.APPLICATION_JSON);
        System.out.println("     stamp request part      = " + stampRequestPart);

        MockMultipartFile pdfSource = new MockMultipartFile(
                "pdfSource",
                TEST_PDF_FILENAME,
                APPLICATION_PDF_VALUE,
                buildFileContent(callerClass.getClassLoader(), TEST_PDF_FILENAME)
        );
        MockMultipartHttpServletRequestBuilder multiPartBuilder = multipart(apiTargetPath).file(pdfSource);

        if (base64ImagesMap != null) {
            base64ImagesMap.forEach((imageRef, base64Image) -> {
                byte[] imageBytes = Base64.getDecoder().decode(base64Image);
                MockMultipartFile imgPart = new MockMultipartFile("images", imageRef, IMAGE_PNG_VALUE, imageBytes);
                multiPartBuilder.file(imgPart);
            });
        }

        mockMvc.perform(
                    multiPartBuilder
                            .part(stampRequestPart)
                            .contentType(MULTIPART_FORM_DATA)
                            .accept(APPLICATION_OCTET_STREAM)
            )
            .andExpect(status().isOk())
            .andDo(result -> {
                byte[] finalSignedFileBytes = result.getResponse().getContentAsByteArray();
                assertNotEquals(0, finalSignedFileBytes.length);
                String signatureResult = "build/test-results/intTest_" + callerClass.getSimpleName() + producedTestFileSuffix;
                File signedFile = new File(signatureResult);
                writeByteArrayToFile(signedFile, finalSignedFileBytes);
            });
    }

    public static void callApiV2WithTestData(MockMvc mockMvc, Class<?> callerClass, Path jsonPath, String apiTargetPath, String producedTestFileSuffix) throws Exception {
        byte[] stampRequestContent = Files.readAllBytes(jsonPath);
        String stampRequestContentStr = new String(stampRequestContent);

        // The standard MockMvc#print() method will print the entire response body.
        // When the response is a PDF file, we don't want that.
        // So we fake the logs here :
        System.out.println();
        System.out.println("MockHttpServletRequest:");
        System.out.println("     Request URI = " + apiTargetPath);
        System.out.println("     Method      = POST");
        System.out.println("     Parameters  = " + stampRequestContentStr);

        Part stampRequestPart = new MockPart("request", null, stampRequestContent, MediaType.APPLICATION_JSON);

        System.out.println("     stamp request part      = " + stampRequestPart);
        mockMvc.perform(
                    multipart(apiTargetPath)
                            .file("file", buildFileContent(callerClass.getClassLoader(), TEST_PDF_FILENAME))
                            .part(stampRequestPart)
                            .contentType(MULTIPART_FORM_DATA)
                            .accept(APPLICATION_OCTET_STREAM)
            )
            .andExpect(status().isOk())
            .andDo(result -> {
                byte[] finalSignedFileBytes = result.getResponse().getContentAsByteArray();
                assertNotEquals(0, finalSignedFileBytes.length);
                String signatureResult = "build/test-results/intTest_" + callerClass.getSimpleName() + producedTestFileSuffix;
                File signedFile = new File(signatureResult);
                writeByteArrayToFile(signedFile, finalSignedFileBytes);
            });
    }
}
