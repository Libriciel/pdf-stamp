/*
 * PDF-Stamp
 * Copyright (C) 2017-2024 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.stamp.service;

import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfReader;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.kernel.pdf.StampingProperties;
import coop.libriciel.stamp.error.RestInvalidPdfException;
import coop.libriciel.stamp.model.Logo;
import coop.libriciel.stamp.model.Position;
import coop.libriciel.stamp.model.Row;
import coop.libriciel.stamp.model.Stamp;
import lombok.NonNull;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.rendering.PDFRenderer;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.util.StringUtils;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;

import static coop.libriciel.stamp.model.PositionOrigin.BOTTOM_LEFT;
import static java.util.Collections.singletonList;
import static org.junit.jupiter.api.Assertions.*;


@ExtendWith(SpringExtension.class)
@SpringBootTest
public class StampServiceImplIntegrationTest {

    @Autowired
    private StampService stampService;

    private final String targetIntermediateDir = "src/test/tmp";
    private final String referencePdfsDir = "src/test/resources/reference_result_pdfs";
    private final String referenceImagesDir = "src/test/resources/reference_result_jpg";

    private static final int COLOR_DELTA = 3;

    private static final String rasterImageFormatName = "JPEG";
    private static final String rasterImageExtension = ".jpg";


    private Stamp createBaseStamp() {
        Stamp stamp = new Stamp();
        // Add 1 row to the stamp and don't define position width and height
        Row row1 = new Row();
        row1.setTitle("Envoyé en préfecture le");
        row1.setValue("13/09/2017");

        Row row2 = new Row();
        row2.setTitle("Reçu en préfecture le");
        row2.setValue("13/09/2017");

        Row row3 = new Row();
        row3.setTitle("Affiché le");
        Logo logo = new Logo();
        logo.setImageData(new ByteArrayInputStream(Base64.getDecoder().decode(
                """
                /9j/4AAQSkZJRgABAQEBLAEsAAD/2wBDAAEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQH/2wBDAQEBAQEBAQEBAQEBAQEBAQEBA\
                QEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQH/wAARCAANAGQDAREAAhEBAxEB/8QAGwAAAgIDAQAAAAAAAAAAAAAABwgFCQEEBgr/xAAsEAABBAIBAwMDAwUAAA\
                AAAAAEAQMFBgIHCAkREgATFBYhMSIlQRcjJGJy/8QAGwEAAgIDAQAAAAAAAAAAAAAAAwQFBgACBwH/xAAzEQACAgECBAMGBAcBAAAAAAABAgMEEQUSAAYTIQcxURQiIzJBYRVxgZEWM0JUYpKxNP/\
                aAAwDAQACEQMRAD8A9YvUA572Xiclfp+qqJqbYO07LFPTaC7l5B0zQlRr0KhWQQkk49Ojyljtz5hY5rOMbARQwY2IuWcjYQXnBhSekch8i1uavaLeqXtV0/S60qw79I0C5rtuebaHeMCBoq9RURkJ\
                ksSs7bgI68gDMqdq0YMKio8jDOJJViAHr3yT+gx6kcVt636qXOm45Sjt9t3SL0mKK823HMWDfG1tiSsti5gueTozGtXJeOYGGXxZdelZSLLdey7jRjrGKveui6j4Ycj0+kKNXxY1pnBLmvoWl6fFF\
                g4AY6iIpCzeYEUUqgfNID24TS7abO80IvTMrsT/AKZH7kflwyei+cvIff8AtaG1BW+UnSS2JLy3z85uk6e2DuY7a6V6IyaytZ9bg5nOVFLk6/G5EFvRsjHNs5ZNe0aZFMo8cPX9Y5L5e5f02XWLXK\
                3itShh2dG3q9LSItKNiUH2SOzYgWJ0inl2qJI3J7+6kpwjbdaexmCOzQDOMZTfI4X+sqjdmIXPn24PHUJ5Q2Hg3qSt2jX9n45G7ruljGr1Jr3JXcUHoaizENHoydfrCpsvaobGwFQUbkAMNCxefvj\
                yVgjTHFwDZfTOG5E5er886rZq3qfMMWi04Gs3LHLmkTa/eglfclCsFipzNXWeTeWnk7PHXkTu7DHkqtRVJBLE8xyiiVvZ0fyLsR1drYx2XHulgRwAemzz65s8ydzT8FsWB4CrpqjVIiWu1i408gmN\
                0XaPskw98OiwTkZAXSfDgxZvMSfkXpOaFRlwOulhCf5hTTjUx4jcicl8n6PXn0+fnv8AGL1pYqVfmPQG0alJXhG69MJLFOu8zQ7oI1jhbIawrt7inJKdqxYkKutUIi5YxTCVsn5RhScZ79z6evF5X\
                rifEnxT9z36q9c4v2QvUumi+Lmwd0xDbX1rD7t5g6j0DE63INDCk4kGchJ46QvE5MSUScNKtxwkNDRzUcWG+5YkIe+EnW+RfC6xzNWXVdYTmeho0pPsU2i8o6rr0uohHeKV4JoEjpQxRyo0RkaaaQ\
                yI6ivgb+ELN0QnpxdB5B8wksRxBM9xkH3iSO+AB2+vHFcZuQXWG5Na+F2tDUnpk1fX8/mVlSZ8Da23dtgXIEQl8EiVh5nWJsrWXIpmSFMjFJWc+bkYEV+1ts4tOPO8yaB4R8t6g2lzXfEm1fgC+2w\
                Ppek6TJTkdVdYpodTSKyJTEyy7ehs2unxScgaQy6hMnU20lU/Kd8j7h65TK4+nn+nEpwv6h3I7a/JK68eeQtV4WvCVk28Q+GyuMPKWHu+QM9QznQT46warsfe5gDGEMvhukPlgzFelWsY+ZrWODr5\
                oI+cPD7l/TOXafMHL1jnPdZSlN+G8ycrT098F5A6SQapWzSkZFKuFVXhsREyQ2MqEfyC85maGwai7dw3x2UPdfoUYhu/7j6r5kXDn2SvxYWcjITMaKA0ndwx0tlBsPv2TyeTJcEXJVTHDHv5Z5KmO\
                CZZKieuSRU7U8ogirzPM3lEqNvP5LjPb6+gyT2HDzzwxp1HlRUHm5Ybf38vsPU9h34HwG24iedIWvpHLFie/wDJmrDOgQA/gN3V50WMy+ZOutYYouXumxsYPkiL4v8AZFX1LS6BYqqgt9brybNlWp\
                VltPl/lDzjZVVjke7HNM4z3XhBNUinJ6HT6a53TTzJCuF8ysZ3TED1eOMffHEpH3OZs/jnUa8r0Sqfa0WB16Hii+y9ldho5B35iVHX8tEPMRYjyfdovPH9XoE2m16ORfthbH9jUVbM8f8AjZm3pXg\
                cfVFaeRf6oxwWO3LY/wDNATF/cTkxRt94o9pllHoxEan6MeCEOhGLOCFOMukdv7mY7ObDK5f6NOPk54p/09mq/n7fj1Ett3HYGC/QMQzY+5CqD+ijh4ZwN2CfrgYH6Ak/94Q678JL7c7jZraN1Bed\
                tIHscybLsU6j23j9HU6sMluebUJWQZbjpOSosNHt+LAbclMyp/hj5lyBT+eb2V4pc50adStVbkLke61eFIjcu1NfkuWSowZrLxcwwxNM57uY4Ykz8sajA4WaszMW9qtLk/KrRBR9h8InH5k8ceT09\
                b4aw6KT1Leo98d/FW3kE2Rx9jCVbyTtkjMhGcaRJATNUX9L4hLD7a/qbcxyRFRxef6KMGXw48O9w7jfp2vyrn7xycyPG4/xdSp8iCONfZG/vLn+8X/ejkfpxv8ADzpY8ROE1ytG1NX1q23Ddd0WY+\
                pt4bkuMjsfaMlhYj8ZWw4tzUhgLHRLlhk0+fYjYSHjZGwkr5zhkgjbOLY+bvE/mznOnV0vU7NWpo1Lo+y6JpFOPTtMi9nj6Vf4Me6SUV4vh10mmkjrr/ISPLZyvRr1mLoGaRs5kkYu5z3Pc+WT3OB\
                3+vGxz06YXF/qLjUBd/D36PntYpPs02264t30zOx0faFi852KIYkYywVyUBPfhIkn9zgCyhHwscgChcXy8CNeRfEvmbw9a/8AgLUJINS6BuVNRqe0wSSVuqIJVMckFiJ0WaVfhTorB/fVtqlfbVKG\
                3t6u4FM7SjYPfGR3BB8vqPy4V3j90PNR8UsrKvG7mNzv023cs4521i1LZWmso+wPRGBLUUTJxk3oKXjnjI5k0tkU3EVsttkl1n31ay8Es+v+Nerc0+zfxFyhyNq5p9QVWtabrAkriUqZVjkg16GQJ\
                IUQum4oSoOM9+AxabHBnoz2o8+e148HHlkGIjtw4+veF13o14q1xk+e/OPZAFblmpQmhX+3aHIpNrwZaewxirQPV+PlWsBETk45gQ8xF2KIfecHabcJUbJ9h6n3+caV2lapx8i8k6c9iIxLeoVNdW\
                7VJIPVrNZ5gtV1lwCoMteVQGJC7trA6VmVg3tVp8HO12jKt9jiIHH5EftwmXKPoC8E+Vm6bzvu1nbyod92VK4T92/pvsGIEgZuwfCFAImlh7nTrtjGlnMBjqYzDPx0c4/hkRgC2667lnceWfHjnjl\
                bRqOhVU0S9R06LoUvxLT5Xnhr73dYetTuUjIiM7bDMJJAMKXIAAXm0urPI0rdRWc5bYwAJwBnDK2PL6Y4JFB6SoOrdbw+ntd9QTqOUzV9ejcYWAp9d3NqaNHgYXHyTCJgJprQ31PBR7KZ5YiDxE4H\
                iDgqYA/GxwwTGOv+Kr6pqM2r6hyD4eXNTsSdae3Y0fVZDPN2zLPAdd9lndse+0sDl/692TwRKIjQRpatqg8lEkYwPQHpbgPTB4M/C7pf8QOBzVqL0hRZY+43oRI257J2VYzr7fLDFIYkksO/JSaNx\
                kZFPyaJJyAEBDxA8vJYMHzGB5Qgbo8Pzh4m8388NVGs3446lF+pT0/TYEoUq8uzp9ZY4viySrH8OOSeaVoYy0cJjRnDbQ0KsAYLHv3jDtL8QsPQ7u2PXAGfrnhvGtP68HnR58WvDCkj5ZOoCOuTcM\
                4T3TJotyI7qF742SeQ6ttNt4OL7yt5PYNON1o8w6u9V6j3HkR8Dqv71kJjBjFj+btcdnyxJHu52lgRDSqCzidYFVgc7F7RFvMMYvk3Ke64Awe+MgETM1rukWE8WTmKzFGHiENktlKOjTrrjWSZ4Ym\
                Kz7aHspkiKo5yEML/AC2v39L1tX1OpFJDXuzxxSIUMYclQGGCYw2ek2O2+LY334NNQpzuskteNnRgwbbgkjy34xvH+L5H247NERERERERE7IifZERPwiJ/CJ6jeG+M+s4zj//2Q=="""
        )));
        logo.setWidth(60);
        logo.setMarginRight(30);
        row3.setLogo(logo);

        Row row4 = new Row();
        row4.setTitle("ID :");
        row4.setValue("007-210703468-20170907-DEC2017_017SG15-AU");


        stamp.setOpacity(0.8f);
        stamp.setFontSize(7);
        stamp.setRows(Arrays.asList(row1, row2, row3, row4));
//        stamp.setSpacesBetweenLines(6);
        return stamp;
    }


    private Position createBasePosition() {
        Position position = new Position();
        position.setX(24);
        position.setY(22);
        position.setWidth(185);
        position.setHeight(52);
        return position;
    }


    /**
     * Generates the full list of stamp's Position that will be considered for this test class.
     * Each position is associated with a name, for convenience when generating files.
     *
     * @return
     */
    private ArrayList<Pair<String, Position>> getAllPositions() {
        ArrayList<Pair<String, Position>> res = new ArrayList<>();
        Position p = this.createBasePosition();
        Pair<String, Position> aPair = new ImmutablePair<>("top_right", p);
        res.add(aPair);

        p = createBasePosition();
        p.setOrigin(BOTTOM_LEFT);
        aPair = new ImmutablePair<>("bottom_left", p);
        res.add(aPair);

        p = createBasePosition();
        p.setCentered(true);
        aPair = new ImmutablePair<>("centered", p);
        res.add(aPair);

        p = createBasePosition();
        p.setOnText("Hello");
        aPair = new ImmutablePair<>("on_text", p);
        res.add(aPair);

        return res;
    }


    /**
     * From the given source pdf, generate stamped versions for all the stamp Position considered.
     *
     * @param srcPdf
     * @param resultBaseName
     * @param baseDir
     * @return
     * @throws IOException
     */
    private ArrayList<String> generateAllStampedPdfForSourceFile(String srcPdf, String resultBaseName, String baseDir) throws IOException {

        ArrayList<String> generatedFilenames = new ArrayList<>();
        for (Pair<String, Position> pair : getAllPositions()) {

            String positionLabel = pair.getKey();
            Stamp stamp = createBaseStamp();
            stamp.setPosition(pair.getValue());

            String targetFilename = resultBaseName + "-stamped-" + positionLabel + ".pdf";

            try (FileInputStream is = new FileInputStream(srcPdf);
                 PdfReader reader = new PdfReader(is);
                 FileOutputStream out = new FileOutputStream(targetFilename);
                 PdfWriter writer = new PdfWriter(out);
                 PdfDocument document = new PdfDocument(reader, writer, new StampingProperties().preserveEncryption())) {

                stampService.addAnnotation(document, singletonList(stamp));
                generatedFilenames.add(targetFilename);

            } catch (IOException e) {
                e.printStackTrace();
                fail("Exception while adding stamp to pdf : " + e);
            }
        }
        return generatedFilenames;
    }


    private ArrayList<String> generateStampedPdfFiles(String targetDir) throws Exception {
        ArrayList<String> allGeneratedPdfFilenames;
        allGeneratedPdfFilenames = generateAllStampedPdfForSourceFile("src/test/resources/small-cropbox.pdf", "cropbox", targetDir);
        allGeneratedPdfFilenames.addAll(generateAllStampedPdfForSourceFile("src/test/resources/cropbox-rotated90.pdf", "cropbox-rotated90", targetDir));
        allGeneratedPdfFilenames.addAll(generateAllStampedPdfForSourceFile("src/test/resources/cropbox-rotated180.pdf", "cropbox-rotated180", targetDir));
        allGeneratedPdfFilenames.addAll(generateAllStampedPdfForSourceFile("src/test/resources/cropbox-rotated270.pdf", "cropbox-rotated270", targetDir));

        return allGeneratedPdfFilenames;
    }


    /**
     * Use org.apache.pdfbox to produce a rendering of the first page of a pdf
     *
     * @param srcPath    the path of the pdf file to raster
     * @param targetPath the path where to write the produced image
     * @throws IOException
     */
    private void renderPdfFileToImage(String srcPath, String targetPath) throws IOException {

        File file = new File(srcPath);

        try (PDDocument doc = PDDocument.load(file)) {
            PDFRenderer renderer = new PDFRenderer(doc);
            BufferedImage image = renderer.renderImage(0, 2.0f);
            ImageIO.write(image, rasterImageFormatName, new File(targetPath));
        }
    }


    /**
     * Takes a list of pdf files in a directory, and for each generate a raster image of the first page.
     * The generated images are placed in the target directory, with the same name as the source file (replacing the '.pdf' extenion by
     * the static-defined rasterImageExtension)
     *
     * @param srcDir    Directory from which to read the source files
     * @param targetDir the target directorry, where image files will be written
     * @param fileNames list of the source pdf file names
     * @return
     * @throws IOException
     */
    private ArrayList<String> generateImagesFromPdfFiles(String srcDir, String targetDir, ArrayList<String> fileNames) throws IOException {
        ArrayList<String> generatedFilenames = new ArrayList<>();

        if (!Files.exists(Paths.get(targetDir))) {
            Files.createDirectories(Paths.get(targetDir));
        }

        for (String srcFilename : fileNames) {
            String targetFilename = StringUtils.stripFilenameExtension(srcFilename) + rasterImageExtension;
            try {
                renderPdfFileToImage(srcDir + "/" + srcFilename, targetDir + "/" + targetFilename);
                generatedFilenames.add(targetFilename);
            } catch (IOException e) {
                e.printStackTrace();
                fail("Exception while rendering pdf to image '" + targetFilename + "' : " + e);
            }
        }
        return generatedFilenames;
    }


    /**
     * Checking pixel's color differences, between both images,
     * One pixel out of 100 is tested, out of performance, and a color delta is used, allowing small color differences.
     *
     * @param bufferedImageLeft  The expected one.
     * @param bufferedImageRight The actual one.
     */
    private static void assertImagesEquals(@NonNull BufferedImage bufferedImageLeft, @NonNull BufferedImage bufferedImageRight) {

        int cmpStep = 10;

        for (int x = 0; x < bufferedImageLeft.getWidth(); x += cmpStep) {
            for (int y = 0; y < bufferedImageLeft.getHeight(); y += cmpStep) {

                int pixelBeforeRgb = bufferedImageLeft.getRGB(x, y);
                int pxBeforeR = (pixelBeforeRgb & 0x00ff0000) >> 16;
                int pxBeforeG = (pixelBeforeRgb & 0x0000ff00) >> 8;
                int pxBeforeB = pixelBeforeRgb & 0x000000ff;

                int pixelAfterRgb = bufferedImageRight.getRGB(x, y);
                int pxAfterR = (pixelAfterRgb & 0x00ff0000) >> 16;
                int pxAfterG = (pixelAfterRgb & 0x0000ff00) >> 8;
                int pxAfterB = pixelAfterRgb & 0x000000ff;

                assertEquals(pxBeforeR, pxAfterR, COLOR_DELTA);
                assertEquals(pxBeforeG, pxAfterG, COLOR_DELTA);
                assertEquals(pxBeforeB, pxAfterB, COLOR_DELTA);
            }
        }
    }


    public static void loadAndCompareImageFiles(Path referenceImagePath, Path testedImagePath) throws IOException {

        InputStream referenceImageStream = Files.newInputStream(referenceImagePath);
        InputStream testedImageStream = Files.newInputStream(testedImagePath);
        assertNotNull(referenceImageStream);
        assertNotNull(testedImageStream);

        BufferedImage referenceImage = ImageIO.read(referenceImageStream);
        BufferedImage testedImage = ImageIO.read(testedImageStream);

        // Tests
        assertEquals(testedImage.getWidth(), referenceImage.getWidth());
        assertEquals(testedImage.getHeight(), referenceImage.getHeight());
        assertImagesEquals(referenceImage, testedImage);

        // Cleanup
        testedImageStream.close();
        referenceImageStream.close();
    }


    @BeforeEach
    public void setup() throws Exception {
        Path tmpDirPath = Paths.get(targetIntermediateDir);
        if (!Files.exists(tmpDirPath)) {
            Files.createDirectories(tmpDirPath);
        }
    }


    @AfterEach
    public void cleanup() throws Exception {
        // TODO we should probably cleanup the tmp dir
    }


    // Deactivated, there's a problem on CI, likely due to fonts not available on system...
    // we should probably make another test-set using only natively available fonts
    @Test
    public void generateAndCompareAll() throws Exception {
//        ArrayList<String> allPdfFilenames = generateStampedPdfFiles(targetIntermediateDir);
//        ArrayList<String> allImagesFilenames = generateImagesFromPdfFiles(targetIntermediateDir, targetIntermediateDir, allPdfFilenames);
//
//        for (String imageFilename : allImagesFilenames) {
//
//            Path referenceImagePath = Paths.get(referenceImagesDir, imageFilename);
//            Path testedImagePath = Paths.get(targetIntermediateDir, imageFilename);
//            loadAndCompareImageFiles(referenceImagePath, testedImagePath);
//        }
        assert (true);
    }


    //    @Test
    // Run only when some valid changes have been made to the stamp position code.
    // Then commit the result images in reference_result_jpg.
    public void generateAllReferences() throws Exception {
        ArrayList<String> allPdfFilenames = generateStampedPdfFiles(referencePdfsDir);
        ArrayList<String> allImagesFilenames = generateImagesFromPdfFiles(referencePdfsDir, referenceImagesDir, allPdfFilenames);
    }

}
