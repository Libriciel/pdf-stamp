/*
 * PDF-Stamp
 * Copyright (C) 2017-2024 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.stamp.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.itextpdf.kernel.geom.Rectangle;
import com.itextpdf.kernel.pdf.*;
import coop.libriciel.stamp.model.Logo;
import coop.libriciel.stamp.model.Position;
import coop.libriciel.stamp.model.Row;
import coop.libriciel.stamp.model.Stamp;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.io.*;
import java.util.*;

import static java.nio.charset.StandardCharsets.UTF_8;


@SpringBootTest
@ExtendWith(SpringExtension.class)
public class StampServiceImplTest {

    private static Map<String, Rectangle> EXPECTED_POSITIONS;


    /*
      Ces vérifications sont prévues pour les valeurs suivantes :
      - width: 185,
      - height: 52,
      - x: 24,
      - y: 22
     */
    static {
        EXPECTED_POSITIONS = new HashMap<>(12);
        EXPECTED_POSITIONS.put("0topRight", new Rectangle(91, 70, 276, 122));
        EXPECTED_POSITIONS.put("90topRight", new Rectangle(22, -65, 74, 120));
        EXPECTED_POSITIONS.put("180topRight", new Rectangle(24, 22, 209, 74));
        EXPECTED_POSITIONS.put("270topRight", new Rectangle(226, 24, 278, 209));

        EXPECTED_POSITIONS.put("0bottomLeft", new Rectangle(24, 22, 209, 74));
        EXPECTED_POSITIONS.put("90bottomLeft", new Rectangle(226, 24, 278, 209));
        EXPECTED_POSITIONS.put("180bottomLeft", new Rectangle(91, 70, 276, 122));
        EXPECTED_POSITIONS.put("270bottomLeft", new Rectangle(22, -65, 74, 120));

        EXPECTED_POSITIONS.put("90centered", new Rectangle(0, -46, 52, 144));
        EXPECTED_POSITIONS.put("180centered", new Rectangle(0, 0, 190, 52));

        EXPECTED_POSITIONS.put("0onText", new Rectangle(-49, -19, 140, 32));
        EXPECTED_POSITIONS.put("90onText", new Rectangle(19, -88, 71, 101));
        EXPECTED_POSITIONS.put("cantOnText", new Rectangle(405, 789, 595, 841));
        EXPECTED_POSITIONS.put("canOnText", new Rectangle(36, 684, 226, 736));
    }


    @Autowired
    private StampService stampService;


    public void assertAnnotationPosition(PdfDocument reader, Rectangle expectedRect, int annotNumber) {
        // Get rectangle from annotation and test position
        PdfArray rectangleArray = reader.getPage(1).getPdfObject().getAsArray(PdfName.Annots).getAsDictionary(annotNumber).getAsArray(PdfName.Rect);

        Rectangle rectangle = new Rectangle(
                rectangleArray.getAsNumber(0).intValue(),
                rectangleArray.getAsNumber(1).intValue(),
                rectangleArray.getAsNumber(2).intValue(),
                rectangleArray.getAsNumber(3).intValue());
        // Assert with 1 pixel epsilon
        assert rectangle.equalsWithEpsilon(expectedRect);
    }


    public void verifyMultipleAnnotationsWithFile(String path, List<String> expected) throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        List<Stamp> stamps = new ArrayList<>();
        stamps.add(mapper.readValue(new File("src/test/resources/sample-s2low-topRight.json"), Stamp.class));
        stamps.add(mapper.readValue(new File("src/test/resources/sample-s2low-bottomLeft.json"), Stamp.class));
        verifyAnnotationWithFile(path, expected.get(0), stamps, 0);
        verifyAnnotationWithFile(path, expected.get(1), stamps, 1);
    }


    public void verifyCenteredAnnotationWithFile(String path, String expected) throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        Stamp stamp = mapper.readValue(new File("src/test/resources/sample-s2low-centered.json"), Stamp.class);
        verifyAnnotationWithFile(path, expected, Collections.singletonList(stamp), 0);
    }


    public void verifyOnTextAnnotationWithFile(String path, String expected) throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        Stamp stamp = mapper.readValue(new File("src/test/resources/sample-s2low-ontext.json"), Stamp.class);
        verifyAnnotationWithFile(path, expected, Collections.singletonList(stamp), 0);
    }


    public void verifySimpleOnTextAnnotationWithFile(String path, String expected) throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        Stamp stamp = mapper.readValue(new File("src/test/resources/sample-s2low-ontext-simple.json"), Stamp.class);
        verifyAnnotationWithFile(path, expected, Collections.singletonList(stamp), 0);
    }


    public void verifyBottomLeftAnnotationWithFile(String path, String expected) throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        Stamp stamp = mapper.readValue(new File("src/test/resources/sample-s2low-bottomLeft.json"), Stamp.class);
        verifyAnnotationWithFile(path, expected, Collections.singletonList(stamp), 0);
    }


    public void verifyTopRightAnnotationWithFile(String path, String expected) throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        Stamp stamp = mapper.readValue(new File("src/test/resources/sample-s2low-topRight.json"), Stamp.class);
        verifyAnnotationWithFile(path, expected, Collections.singletonList(stamp), 0);
    }


    public void verifyAnnotationWithFile(String path, String expected, List<Stamp> stamps, int annotNumber) throws Exception {

        String resultPath = "build/test-results/verifyAnnotationWithFile_" + expected + ".pdf";
        InputStream is = new FileInputStream(path);
        FileOutputStream bout = new FileOutputStream(resultPath);
        PdfWriter writer = new PdfWriter(bout);
        PdfReader reader = new PdfReader(is);
        PdfDocument document = new PdfDocument(reader, writer, new StampingProperties().preserveEncryption());

        stampService.addAnnotation(document, stamps);
        assertAnnotationPosition(document, EXPECTED_POSITIONS.get(expected), annotNumber);
    }


    @Test
    public void whenRotatedPDF_thenRotatedAnnot() throws Exception {
        verifyTopRightAnnotationWithFile("src/test/resources/minimal.pdf", "0topRight");
        verifyTopRightAnnotationWithFile("src/test/resources/minimal90.pdf", "90topRight");
        verifyTopRightAnnotationWithFile("src/test/resources/minimal180.pdf", "180topRight");
        verifyTopRightAnnotationWithFile("src/test/resources/minimal270.pdf", "270topRight");
    }


    @Test
    public void whenRotatedPDF_withBottomLeftPlacement_thenRotatedAnnot() throws Exception {
        verifyBottomLeftAnnotationWithFile("src/test/resources/minimal.pdf", "0bottomLeft");
        verifyBottomLeftAnnotationWithFile("src/test/resources/minimal90.pdf", "90bottomLeft");
        verifyBottomLeftAnnotationWithFile("src/test/resources/minimal180.pdf", "180bottomLeft");
        verifyBottomLeftAnnotationWithFile("src/test/resources/minimal270.pdf", "270bottomLeft");
    }


    @Test
    public void whenNoSizeInPosition_thenHandleItProperly() throws Exception {
        Stamp stamp = new Stamp();
        // Add 1 row to the stamp and don't define position width and height
        Row row = new Row();
        row.setTitle("Test");
        row.setValue("Value");

        Position position = new Position();
        position.setX(24);
        position.setY(22);
        position.setMarginLeft(6);

        stamp.setOpacity(1);
        stamp.setFontSize(7);
        stamp.setPosition(position);
        stamp.setRows(Collections.singletonList(row));
        stamp.setSpacesBetweenLines(6);

        FileInputStream is = new FileInputStream("src/test/resources/minimal.pdf");
        PdfWriter writer = new PdfWriter(new ByteArrayOutputStream());
        PdfReader reader = new PdfReader(is);
        PdfDocument document = new PdfDocument(reader, writer, new StampingProperties().preserveEncryption());

        stampService.addAnnotation(document, Collections.singletonList(stamp));
        Rectangle expectedPosition = new Rectangle(230, 107, 276, 122);
        assertAnnotationPosition(document, expectedPosition, 0);
    }


    @Test
    public void whenBadFileFormat_thenHandleItProperly() throws Exception {
        Stamp stamp = new Stamp();
        // Add 1 row to the stamp and don't define position width and height
        Logo logo = new Logo();
        logo.setImageData(new ByteArrayInputStream("test".getBytes(UTF_8)));

        Row row = new Row();
        row.setLogo(logo);

        Position position = new Position();
        position.setX(24);
        position.setY(22);
        position.setMarginLeft(6);

        stamp.setOpacity(1);
        stamp.setFontSize(7);
        stamp.setPosition(position);
        stamp.setRows(Collections.singletonList(row));
        stamp.setSpacesBetweenLines(6);

        FileInputStream is = new FileInputStream("src/test/resources/minimal.pdf");
        PdfWriter writer = new PdfWriter(new ByteArrayOutputStream());
        PdfReader reader = new PdfReader(is);
        PdfDocument document = new PdfDocument(reader, writer, new StampingProperties().preserveEncryption());

        stampService.addAnnotation(document, Collections.singletonList(stamp));

        Rectangle expectedPosition = new Rectangle(240, 107, 276, 122);
        assertAnnotationPosition(document, expectedPosition, 0);
    }


    @Test
    public void whenNotEnoughPages_thenHandleItProperly() throws Exception {
        Stamp stamp = new Stamp();
        // Add 1 row to the stamp and don't define position width and height
        Row row = new Row();
        row.setTitle("Test");
        row.setValue("Value");

        Position position = new Position();
        position.setX(24);
        position.setY(22);
        position.setMarginLeft(6);

        stamp.setPage(3);
        stamp.setOpacity(1);
        stamp.setFontSize(7);
        stamp.setPosition(position);
        stamp.setRows(Collections.singletonList(row));
        stamp.setSpacesBetweenLines(6);

        FileInputStream is = new FileInputStream("src/test/resources/minimal.pdf");
        PdfWriter writer = new PdfWriter(new ByteArrayOutputStream());
        PdfReader reader = new PdfReader(is);
        PdfDocument document = new PdfDocument(reader, writer, new StampingProperties().preserveEncryption());

        stampService.addAnnotation(document, Collections.singletonList(stamp));
    }


    @Test
    public void whenMultipleStamps_thenPlaceThemProperly() throws Exception {
        List<String> expected = new ArrayList<>();
        expected.add("0topRight");
        expected.add("0bottomLeft");
        verifyMultipleAnnotationsWithFile("src/test/resources/minimal.pdf", expected);
    }


    @Test
    public void whenCenteredStamp_thenPlaceItProperly() throws Exception {
        verifyCenteredAnnotationWithFile("src/test/resources/minimal90.pdf", "90centered");
        verifyCenteredAnnotationWithFile("src/test/resources/minimal180.pdf", "180centered");
    }


    @Test
    public void whenOnTextStamp_thenPlaceItProperly() throws Exception {
        verifyOnTextAnnotationWithFile("src/test/resources/minimal.pdf", "0onText");
        verifyOnTextAnnotationWithFile("src/test/resources/minimal90.pdf", "90onText");
        verifySimpleOnTextAnnotationWithFile("src/test/resources/cantlocate.pdf", "cantOnText");
        verifySimpleOnTextAnnotationWithFile("src/test/resources/canlocate.pdf", "canOnText");
    }
}
