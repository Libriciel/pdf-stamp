/*
 * PDF-Stamp
 * Copyright (C) 2017-2024 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.stamp.model;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;


class StampTest {


    @Test
    void deserialize() throws JsonProcessingException {

        String json =
                """
                {
                  "page":4,
                  "position":{
                    "x":10,
                    "y":20,
                    "width":150,
                    "height":200,
                    "marginLeft":5,
                    "placement":"TOP_RIGHT",
                    "onText":null,
                    "centered":false
                  },
                  "opacity":0.0,
                  "rows":[
                    {
                      "title":"Row 1 title",
                      "value":"Row 1 value",
                      "logo":null,
                      "color":"black"
                    },
                    {
                      "title":"Row 2 title",
                      "value":"Row 2 value",
                      "logo":null,
                      "color":"red"
                    }
                  ],
                  "fontSize":10,
                  "spacesBetweenLines":0,
                  "drawRectangle":false
                }
                """;

        Stamp stamp = new ObjectMapper().readValue(json, Stamp.class);

        assertNotNull(stamp);
        assertEquals(stamp.getPage(), 4);
        assertEquals(stamp.getFontSize(), 10);
        assertFalse(stamp.isDrawRectangle());

        assertEquals(stamp.getRows().size(), 2);

        Row row1 = stamp.getRows().get(0);
        assertNotNull(row1);
        assertEquals(row1.getTitle(), "Row 1 title");
        assertEquals(row1.getValue(), "Row 1 value");
        assertEquals(row1.getColor(), "black");

        Row row2 = stamp.getRows().get(1);
        assertNotNull(row2);
        assertEquals(row2.getTitle(), "Row 2 title");
        assertEquals(row2.getValue(), "Row 2 value");
        assertEquals(row2.getColor(), "red");

        Position position = stamp.getPosition();
        assertNotNull(position);
        assertEquals(position.getX(), 10);
        assertEquals(position.getY(), 20);
        assertEquals(position.getWidth(), 150);
        assertEquals(position.getHeight(), 200);
    }


}
