PDF-Stamp
=========

[![pipeline status](https://gitlab.libriciel.fr/outils/pdf-stamp/badges/master/pipeline.svg)](https://gitlab.libriciel.fr/outils/pdf-stamp/commits/master)
[![coverage report](https://gitlab.libriciel.fr/outils/pdf-stamp/badges/master/coverage.svg)](https://gitlab.libriciel.fr/outils/pdf-stamp/commits/master)

[![Alerte](https://sonarqube.libriciel.fr/api/project_badges/measure?project=coop.libriciel%3Apdf-stamp&metric=alert_status)](https://sonarqube.libriciel.fr/dashboard?id=coop.libriciel%3Apdf-stamp)
[![Maintenabilité](https://sonarqube.libriciel.fr/api/project_badges/measure?project=coop.libriciel%3Apdf-stamp&metric=sqale_rating)](https://sonarqube.libriciel.fr/dashboard?id=coop.libriciel%3Apdf-stamp)
[![Fiabilité](https://sonarqube.libriciel.fr/api/project_badges/measure?project=coop.libriciel%3Apdf-stamp&metric=reliability_rating)](https://sonarqube.libriciel.fr/dashboard?id=coop.libriciel%3Apdf-stamp)
[![Sécurité](https://sonarqube.libriciel.fr/api/project_badges/measure?project=coop.libriciel%3Apdf-stamp&metric=security_rating)](https://sonarqube.libriciel.fr/dashboard?id=coop.libriciel%3Apdf-stamp)

[![Bugs](https://sonarqube.libriciel.fr/api/project_badges/measure?project=coop.libriciel%3Apdf-stamp&metric=bugs)](https://sonarqube.libriciel.fr/dashboard?id=coop.libriciel%3Apdf-stamp)
[![Mauvaises pratiques](https://sonarqube.libriciel.fr/api/project_badges/measure?project=coop.libriciel%3Apdf-stamp&metric=code_smells)](https://sonarqube.libriciel.fr/dashboard?id=coop.libriciel%3Apdf-stamp)
[![Dette Technique](https://sonarqube.libriciel.fr/api/project_badges/measure?project=coop.libriciel%3Apdf-stamp&metric=sqale_index)](https://sonarqube.libriciel.fr/dashboard?id=coop.libriciel%3Apdf-stamp)

Service de tampon PDF.

Utilisation de la librairie iText pour ajout d'un tampon sous forme de texte ou d'image.

## Packaging

*Pré-requis :*
* Java 8
* Maven 3

Lancer la commande suivante à la racine du projet :
```bash
mvn package -Dmaven.test.skip=true
```

Le jar packagé sera disponible dans `target/pdf-stamp-${VERSION}.jar`.

## Docker

Une image docker est construite à chaque commit.
Cette image ne possède aucune dépendance.

Il suffira de lancer l'image docker du projet `pdf-stamp` de la façon suivante :
```bash
docker run --name pdf-stamp -p 8080:8080 -d gitlab.libriciel.fr:4567/outils/pdf-stamp
```

L'application sera ensuite accessible à l'adresse [http://localhost:8080/](http://localhost:8080/).  
Un Swagger sera disponible à l'adresse : [http://localhost:8080/swagger-ui.html](http://localhost:8080/pdf-stamp/swagger-ui/index.html)


### Variables d'environnement

- **JAVA_OPTS** : Options de lancement. Permet par exemple de définir la mémoire maximale allouée à l'application. Par defaut défini à `-Xmx512m`

## Exemple d'utilisation

Tampon d'un fichier via curl :

```bash
curl -F "file=@fichier_pdf.pdf" -F 'metadata={"opacity":0.5,"fontSize":7,"position":{"width":190,"height":55, "x":10,"y":10},"rows":[{"title":"Envoyé en préfecture le", "value":"13/09/2017"},{"title":"Reçu en préfecture le","value":"13/09/2017"},{"title":"Affiché le","value":""},{"title":"ID :", "value":"007-210703468-20170907-DEC2017_017SG-AU"}]}' -X POST http://localhost:8080/
```
