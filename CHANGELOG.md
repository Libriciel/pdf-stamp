Change Log
==========

Tous les changements notables sur ce projet seront documentés dans ce fichier.

Ce format est basé sur [Keep a Changelog](http://keepachangelog.com/)
et ce projet adhère au [Semantic Versioning](http://semver.org/).


## [Unreleased](https://gitlab.libriciel.fr/outils/pdf-stamp/)


## [2.5.3] - 2022-04-01
[2.5.3]: https://gitlab.libriciel.fr/outils/pdf-stamp/-/tags/2.5.3
### Corrections
- Déploiement automatique


## [2.5.2] - 2022-04-01
[2.5.2]: https://gitlab.libriciel.fr/outils/pdf-stamp/-/tags/2.5.2
### Corrections
- Spring4Shell


## [2.5.1] - 2022-03-11
[2.5.1]: https://gitlab.libriciel.fr/outils/pdf-stamp/-/tags/2.5.1
### Corrections
- Optimisation de la RAM dans le conteneur Docker


## [2.5.0] - 2021-10-28
[2.5.0]: https://gitlab.libriciel.fr/outils/pdf-stamp/-/tags/2.5.0
### Amélioration
- Tampon de commentaires
### Évolution
- Java17


## [2.4.6] - 2021-04-29
[2.4.6]: https://gitlab.libriciel.fr/outils/pdf-stamp/-/tags/2.4.6
### Corrections
- Correction du positionnement sur texte afin de centrer l'image de signature sur le texte


## [2.4.5] - 2020-04-28
[2.4.5]: https://gitlab.libriciel.fr/outils/pdf-stamp/-/tags/2.4.5
### Corrections
- Gestion d'erreur lors de l'ajout d'une image vide
- Gestion de la cropbox des pdf pour le positionnement des tampons


## [2.4.4] - 2020-04-23
[2.4.4]: https://gitlab.libriciel.fr/outils/pdf-stamp/-/tags/2.4.4
### Amélioration
- Packaging automatique


## [2.4.3] - 2020-03-31
[2.4.3]: https://gitlab.libriciel.fr/outils/pdf-stamp/-/tags/2.4.3
### Amélioration
- Déploiement automatique sur hub.docker.com


## [2.4.2] - 2020-02-21
[2.4.2]: https://gitlab.libriciel.fr/outils/pdf-stamp/-/tags/2.4.2
### Améliorations
- Mise à jour de la librairie `auto-update`


## [2.4.1] - 2020-01-02
[2.4.1]: https://gitlab.libriciel.fr/outils/pdf-stamp/-/tags/2.4.1
### Corrections
- Une exception était lancée et bloquait le tampon si un mauvais format d'image était envoyé
- Mauvaise remontée d'erreurs pour les exceptions gérées par Spring
- Ajout de dépendance à bouncycastle pour déchiffrer certains PDF
- Les exceptions lancées pour les fichiers trop imposants sont maintenant gérées correctement
- Les exceptions lancées dans le cas de syntaxe JSON incorrecte sont maintenant gérées correctement
- Meilleure validation du modèle StampList
- Gestion de l'exception "ClientAbort"
### Amélioration
- Ajout d'une variable d’environnement permettant de définir la taille maximale des PDF envoyés (par défaut à 128 Mo)


## [2.4.0] - 2019-11-26
[2.4.0]: https://gitlab.libriciel.fr/outils/pdf-stamp/-/tags/2.4.0
### Évolution
- Mise à jour de la librairie `auto-updater`, permettant l'ajout d'une API de maintenance


## [2.3.1] - 2019-10-24
[2.3.1]: https://gitlab.libriciel.fr/outils/pdf-stamp/-/tags/2.3.1
### Corrections
- Certains PDF n'étaient pas affichés correctement après l'apposition des annotations


## [2.3.0] - 2019-08-19
[2.3.0]: https://gitlab.libriciel.fr/outils/pdf-stamp/-/tags/2.3.0
### Évolutions
- Récupération d'erreurs automatique via l'outil Sentry


## [2.2.1] - 2019-06-20
[2.2.1]: https://gitlab.libriciel.fr/outils/pdf-stamp/-/tags/2.2.1
### Corrections
- Les PDF sans section `xref` n'étaient plus gérés correctement


## [2.2.0] - 2019-05-03
[2.2.0]: https://gitlab.libriciel.fr/outils/pdf-stamp/-/tags/2.2.0
## Évolutions
- Nouveau paramètre sur la position, `onText`, permettant de dessiner l'annotation à la position d'un texte défini.


## [2.1.1] - 2019-04-30
[2.1.1]: https://gitlab.libriciel.fr/outils/pdf-stamp/-/tags/2.1.1
### Corrections
- Mauvaise construction de l'entrée d'API `/multiple`


## [2.1.0] - 2019-04-30
[2.1.0]: https://gitlab.libriciel.fr/outils/pdf-stamp/-/tags/2.1.0
## Évolutions
- Nouvelle entrée d'API `/multiple` permettant d'apposer plusieurs annotations simultanément.


## [2.0.2] - 2019-04-30
[2.0.2]: https://gitlab.libriciel.fr/outils/pdf-stamp/-/tags/2.0.2
### Corrections
- Correction d'erreur si la page spécifiée dans le tampon > le nombre de pages du document.


## [2.0.1] - 2019-04-29
[2.0.1]: https://gitlab.libriciel.fr/outils/pdf-stamp/-/tags/2.0.1
### Corrections
- Problème de déploiement du fichier compilé


## [2.0.0] - 2019-04-29
[2.0.0]: https://gitlab.libriciel.fr/outils/pdf-stamp/-/tags/2.0.0
### Évolutions
- iText mis à jour en version 7.1.5
- Spring Boot mis à jour en version 2.1.2
- Nouveau paramètre général, `drawRectangle`, permettant de dessiner ou non le rectangle de l'annotation. Actif par défaut
- Nouveau paramètre général, `page`, permettant de spécifier sur quelle(s) page(s) positionner l'annotation. Toutes les pages par défaut.
- Nouveau paramètre sur la position, `placement`, permettant de dessiner l'annotation à partir du haut à droite (`TOP_RIGHT`) ou du bas à gauche (`BOTTOM_LEFT`). Haut à droite par défaut.
- Nouveau paramètre sur les lignes, `color`, permettant de définir la couleur du texte à afficher. "black" par défaut
###  Corrections
- Les PDF utilisés par les tests unitaires sont maintenant chiffrés
- Les PDF protégés par mot de passe étaient mal gérés


## [1.3.0] - 2018-01-17
[1.3.0]: https://gitlab.libriciel.fr/outils/pdf-stamp/-/tags/1.3.0
### Évolutions
- Possibilité de renseigner la marge gauche dans la position via la propriété `marginLeft` (5 par défaut)
- L'espace entre chaque ligne est maintenant calculé automatiquement
- Possibilité de renseigner l'espace entre chaque ligne via la propriété `spacesBetweenLines`


## [1.2.1] - 2018-01-16
[1.2.1]: https://gitlab.libriciel.fr/outils/pdf-stamp/-/tags/1.2.1
### Corrections
- Les documents contenant des erreurs sont maintenant tamponnés correctement.


## [1.2.0] - 2018-01-12
[1.2.0]: https://gitlab.libriciel.fr/outils/pdf-stamp/-/tags/1.2.0
### Corrections
- Gestion des documents ayant subis une rotation
### Ajouts
- Message d'erreur plus parlant avec référence au fichier


## [1.1.0] - 2017-12-05
[1.1.0]: https://gitlab.libriciel.fr/outils/pdf-stamp/-/tags/1.1.0
### Évolutions
- L'ajout du tampon n'invalide plus une signature électronique


## [1.0.1] - 2017-11-09
[1.0.1]: https://gitlab.libriciel.fr/outils/pdf-stamp/-/tags/1.0.1
### Ajouts
- Changelog
### Corrections
- Mauvaise gestion des documents avec rotation

