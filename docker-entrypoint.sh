#!/bin/bash

#
# PDF-Stamp
# Copyright (C) 2017-2024 Libriciel-SCOP
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#

echo "running ip-core entrypoint script"

if [[ -n "$*" ]]; then
  echo "running additional commands : $@"
  /bin/ash -c "$*"
fi

echo "running main command"


# Adding '-XX:G1PeriodicGCInterval=30000 -XX:G1PeriodicGCSystemLoadThreshold=1' means that every 30 sec,
# if cpu load average has been is below 2 (addition of each core load) and memory consumption is high, a global GC is triggered
java -XX:+UseG1GC -XX:G1PeriodicGCInterval=30000 -XX:G1PeriodicGCSystemLoadThreshold=2 -XX:+UseContainerSupport -XX:MaxRAMPercentage=75.0 $JAVA_OPTS -Djava.security.egd=file:/dev/./urandom -jar /pdf-stamp.jar
